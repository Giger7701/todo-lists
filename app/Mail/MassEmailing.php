<?php

namespace App\Mail;

use Exception;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class MassEmailing extends Mailable
{
	use Queueable, SerializesModels;

	public $fromValue;
	public $subjectValue;
	private $bodyValue;

	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */
	public function __construct($fromValue, $subjectValue, $bodyValue)
	{
		$this->fromValue = $fromValue;
		$this->subjectValue = $subjectValue;
		$this->bodyValue = $bodyValue;
	}

	/**
	 * The job failed to process.
	 *
	 * @param  Exception  $exception
	 * @return void
	 */
	public function failed(Exception $exception)
	{
		// Send user notification of failure, etc...
		Log::info('(MassEmailing failed job) ' . $exception->getMessage());
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		return $this->from([
			'address' => config('mail.from.address'),
			'name' => $this->fromValue
		])->subject($this->subject)->view('admin.emails.mass_emailing')->with([
			'body' => $this->bodyValue,
			'subject' => $this->subjectValue
		]);
	}
}
