<?php

namespace App;

class APIResponse
{
	// Status
	const STATUS_OK = 'OK';
	const STATUS_USER_ACCOUNT = 'USER_ACCOUNT';
	const STATUS_TOKEN_ERROR = 'TOKEN_ERROR';
	const STATUS_INVALID_REQUEST = 'INVALID_REQUEST';
	const STATUS_NOT_FOUND = 'NOT_FOUND';

	// 2xx Success
	const CODE_OK = 200;
	const CODE_CREATED = 201;
	const CODE_ACCEPTED = 202;
	const CODE_NO_CONTENT = 204;

	// 4xx Client errors
	const CODE_BAD_REQUEST = 400;
	const CODE_UNAUTHORIZED = 401;
	const CODE_FORBIDDEN = 403;
	const CODE_NOT_FOUND = 404;

	private $response = [];
	private $status;
	private $message = NULL;
	private $data = NULL;

	/**
	 * Set a response data set
	 *
	 * @return void
	 */
	public function setData(array $data)
	{
		$this->data = $data;
	}

	/**
	 * Add the response data set to the response object
	 *
	 * @return void
	 */
	private function addData()
	{
		if ($this->data !== NULL) {
			$this->response['data'] = $this->data;
		}
	}

	/**
	 * Add the response status to the response object
	 *
	 * @return void
	 */
	private function addStatus()
	{
		$this->response['status'] = $this->status;
	}

	/**
	 * Add a response message
	 *
	 * @return void
	 */
	public function setMessage($message)
	{
		$this->message = $message;
	}

	/**
	 * Add the message to the response object
	 *
	 * @return void
	 */
	private function addMessage()
	{
		if ( is_string($this->message) ) {
			$this->response['message'] = $this->message;
		}
	}

	/**
	 * Merge all sub-object with the main response object
	 *
	 * @return array The response object
	 */
	public function getResponse()
	{
		$this->addStatus();
		$this->addMessage();
		$this->addData();

		return $this->response;
	}

	/**
	 * API Response class contructor
	 *
	 * @return void
	 */
	public function __construct($status)
	{
		$this->status = $status;
	}
}
