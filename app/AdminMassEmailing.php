<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminMassEmailing extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'admin_mass_emailing';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = false;
}
