<?php

namespace App;

use Illuminate\{
	Database\Eloquent\Model,
	Support\Facades\DB
};

class TasksOrder extends Model
{
	protected $table = 'tasks_order';
	public $timestamps = false;

	/**
	 * Get the tasks order by user id
	 *
	 * @return array An array of ListOrder objects. This should be only one object containing the database table column values as properties.
	 */
	public static function getByUserId($userId)
	{
		/*
		return self::where('user_id', $userId)->get(['order']);
		*/

		$sql = '
			SELECT
				`order`
			FROM tasks_order
			WHERE user_id = ?
		';

		return DB::select($sql, [$userId]);
	}
}
