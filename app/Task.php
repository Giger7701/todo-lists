<?php

namespace App;

use Illuminate\{
	Database\Eloquent\Model,
	Support\Facades\DB,
	Support\Facades\Log
};

use App\Exceptions\InvalidOwnerException;

class Task extends Model
{
	/**
	 * Copy tasks from one list to another
	 *
	 * @param integer $fromListId The list id to copy tasks from
	 * @param integer $toListId   The list id to paste tasks to
	 *
	 * @return bool True on success, False otherwise
	 */
	public static function copyFromTo($fromListId, $toListId)
	{
		/*
		$result = DB::insert('
			INSERT INTO tasks (list_id, name, completed, created_at, updated_at)
			SELECT ?, name, completed, created_at, updated_at FROM tasks WHERE list_id = ?
		',[$toListId, $fromListId]);
		*/

		$result = DB::insert('
			INSERT INTO tasks (list_id, name)
			SELECT ?, name FROM tasks WHERE list_id = ?
		', [$toListId, $fromListId]);

		return $result;
	}

	/**
	 * Validates the owner of the task
	 *
	 * @param $taskId Integer The task id
	 *
	 * @throws InvalidOwnerException In case the task does not exist or the owner is someone else
	 *
	 * @return void
	 */
	public static function validateOwner($taskId, $userId)
	{
		$result = DB::select('
			SELECT TRUE
			FROM tasks as t
			JOIN lists as l ON (l.id = t.list_id)
			WHERE l.user_id = ? AND t.id = ?
		', [$userId, $taskId]);

		if (empty($result)) {
			throw new InvalidOwnerException('Task doesn\'t exist');
		}
	}

	/**
	 * Get all list's tasks
	 *
	 * @param $listId Integer The list id
	 *
	 * @return array
	 */
	public static function getListTasks($listId)
	{
		return DB::select('
			SELECT
				id,
				name,
				completed
			FROM tasks
			WHERE list_id = ?
		', [$listId]);
	}

	/**
	 * Get all list's tasks ordered
	 *
	 * @param $listId    integer The list id
	 * @param $orderList string  The ids to order by
	 *
	 * @see `order` column in table "tasks_order"
	 *
	 * @return array
	 */
	public static function getListTasksOrdered($listId, $orderList)
	{
		// $userId = 1; // temporary here, until implementation of authentication/authorization system

		$sql = "
			SELECT
				id,
				name,
				completed
			FROM tasks
			WHERE list_id = ?
			ORDER BY field (id, ${orderList})
		";

		return DB::select($sql, [$listId]);
	}

	/**
	 * Gets a user's tasks from the database
	 *
	 * @param $userId Integer The user id
	 *
	 * @return array
	 */
	public static function getUserTasks($userId)
	{
		/*
		return DB::table('tasks AS t')->select(['l.id as list_id', 't.id', 't.name', 't.completed'])->join('lists AS l', 'l.id', '=', 't.list_id')->where('l.user_id', $userId)->get();
		*/

		return DB::select('
			SELECT
				l.id as list_id,
				t.id,
				t.name,
				t.completed
			FROM tasks AS t
			JOIN lists AS l ON (l.id = t.list_id)
			WHERE l.user_id = ?
		', [$userId]);
	}
}
