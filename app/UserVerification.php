<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SimpleToken;

class UserVerification extends Model
{
	use SimpleToken;
}
