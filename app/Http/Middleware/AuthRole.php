<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\{
	Support\Facades\Log
};

use App\{
	User,
	APIResponseFactory,
	Exceptions\AuthException
};

class AuthRole
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next, $role1, $role2 = NULL)
	{
		try {
			if ($role2 !== NULL) {
				if ($request->currentUser->type != $role1 && $request->currentUser->type != $role2) {
					throw new AuthException('invalid_user_role');
				}

				/*
				// switch users
				// admin only
				if (
					$request->currentUser instanceof User
					&&
					$request->additionalUser instanceof User
				) {
					$request->currentUser = $request->additionalUser;
					unset($request->additionalUser);
				}
				*/
			}
			else if ($request->currentUser->type != $role1) {
				throw new AuthException('invalid_user_role');
			}

		}
		catch (AuthException $ex) {
			return APIResponseFactory::makeForbiddenAccount($ex);
		}

		return $next( $request );
	}
}
