<?php

namespace App\Http\Middleware;

use Validator;
use Closure;

use Illuminate\{
	Support\Facades\Log,
	Support\Facades\Input
};

use JWTAuth;
use Tymon\JWTAuth\Exceptions\{
	TokenExpiredException,
	TokenInvalidException,
	JWTException
};

use App\{
	User,
	APIResponse,
	APIResponseFactory,
	Exceptions\ForbiddenException
};

class AuthCheck
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		try {

			// get the token from the request header
			$token = JWTAuth::parseToken()->getToken();

			// decode the token
			$tokenDecoded = JWTAuth::decode($token);

			// find the user
			$user = User::find($tokenDecoded['sub']);
			if (empty($user)) {
				throw new ForbiddenException('user_not_found');
			}

			// user account inactive?
			if ($user->active == 0) {
				throw new ForbiddenException('account_inactive');
			}

			// user account verified?
			if ($user->verified == 0) {
				throw new ForbiddenException('account_not_verified');
			}

			/*
			// there is no additional login
			if (empty($tokenDecoded['loggedInAs']['userId'])) {
				// only normal user need this check(s)
				// user account inactive?
				if ($user->active == 0) {
					throw new ForbiddenException('account_inactive');
				}

				// user account verified?
				if ($user->verified == 0) {
					throw new ForbiddenException('account_not_verified');
				}
			}
			// additional login!
			// only admin is using this by adding an "Audience" content to the token
			// for more info see: App\Http\Controllers\Admin\UsersController->loginAsThisUser()
			else {
				$additionalUser = User::find($tokenDecoded['loggedInAs']['userId']);
				if ( ! empty($additionalUser)) {
					$request->additionalUser = $additionalUser;
				}
			}
			*/

			// refresh the token
			// $refreshedToken = JWTAuth::refresh($token);

			// storing the data for future use
			$request->currentUser = $user;
			// $request->currentToken = $refreshedToken;

			/*
			// check for additional login info
			// this is used only by the admin panel as "login as this user" functionality
			// for more info see: https://github.com/tymondesigns/jwt-auth/wiki/Creating-Tokens#creating-a-token-based-on-anything-you-like
			if ( ! empty($tokenDecoded['aud'])) {
				$audValue = $tokenDecoded['aud'];
				if ( ! empty($tokenDecoded[$audValue])) {

				}
			}
			*/

			/*
			// OLD METHOD

			// get the token from the request header
			$token = JWTAuth::parseToken()->getToken();

			// authenticate
			$user = JWTAuth::toUser($token);

			// user not found
			if (empty($user)) {
				throw new ForbiddenException('user_not_found');
			}

			// user account inactive
			if ($user->active == 0) {
				throw new ForbiddenException('account_inactive');
			}

			// user account is verified
			if ($user->verified == 0) {
				throw new ForbiddenException('account_not_verified');
			}

			// refresh the token
			// $refreshedToken = JWTAuth::refresh($token);

			// storing the data for future use
			$request->currentUser = $user;
			// $request->currentToken = $refreshedToken;
			*/
		}
		catch (ForbiddenException $ex) {
			return APIResponseFactory::makeForbiddenAccount($ex);
		}
		catch (TokenExpiredException $ex) {
			// token_expired
			return APIResponseFactory::makeForbiddenToken($ex);
		}
		catch (TokenInvalidException $ex) {
			// token_invalid
			return APIResponseFactory::makeForbiddenToken($ex);
		}
		catch (JWTException $ex) {
			// token_absent
			return APIResponseFactory::makeForbiddenToken($ex);
		}

		return $next( $request );
	}
}
