<?php

namespace App\Http\Controllers;

use Validator;

use Illuminate\{
	Http\Request,
	Database\Eloquent\ModelNotFoundException,
	Support\Facades\DB,
	Support\Facades\Log
};

use App\{
	APIResponse,
	APIResponseFactory,
	Exceptions\InvalidOwnerException,
	Exceptions\ValidationException,
	Lists,
	Task,
	TaskTemplates,
	ListTemplates,
	ListsOrder,
	UserPhotos,
	TasksOrder,
	Traits\OrderItems,
	Traits\Validations
};

class ListsController extends Controller
{
	use OrderItems;
	use Validations;

	/**
	 * Update User's list order
	 *
	 * @param $request Request
	 *
	 * @return object Response
	 */
	public function updateOrder(Request $request)
	{
		try {
			$validator = Validator::make( $request->all(), [
				'listsOrder' => 'required|regex:' . $this->orderRegEx
			]);
			$this->checkForErrors($validator);

			$listsOrder = ListsOrder::where('user_id', $request->currentUser->id)->first();
			$listsOrder->order = $request->input('listsOrder');
			$listsOrder->save();

		} catch (ValidationException $ex) {
			return APIResponseFactory::makeValidationError($ex);
		}

		$apiResponse = new APIResponse(APIResponse::STATUS_OK);
		$apiResponse->setData([
		]);
		return response()->json($apiResponse->getResponse(), APIResponse::CODE_OK);
	}

	/**
	 * Rename a list and/or add new taks from templates
	 *
	 * @param $request Request
	 *
	 * @throws ValidationException In case the user's data is not valid or missing
	 * @throws InvalidOwnerException In case the task is not owned by the current user
	 *
	 * @return object Response
	 */
	public function partialUpdate(Request $request)
	{
		try {
			$validator = Validator::make( $request->all(), [
				'listId' => 'required|integer|min:1',
				'listName' => 'required|string|min:1|max:96',
				'templateId' => 'required|integer',
			]);
			$this->checkForErrors($validator);

			Lists::validateOwner($request->input('listId'), $request->currentUser->id);

			// create a new list
			$list = Lists::find($request->input('listId'));
			$list->name = $request->input('listName');
			$list->update();
			$list->refresh();

			// manage template (adding the tasks, if need)
			$this->manageTemplate($list->id, $request->input('templateId'));

			// add the new tasks
			$newList = $this->prepareNewList($list);

		} catch (ValidationException $ex) {
			return APIResponseFactory::makeValidationError($ex);
		} catch (InvalidOwnerException $ex) {
			return APIResponseFactory::makeInvalidOwner($ex);
		}

		$apiResponse = new APIResponse(APIResponse::STATUS_OK);
		$apiResponse->setData([
			'list' => $newList
		]);
		return response()->json($apiResponse->getResponse(), APIResponse::CODE_OK);
	}

	/**
	 * Get list templates
	 *
	 * @param $request Request
	 *
	 * @return object Request
	 */
	public function getTemplates(Request $request)
	{
		$lists = ListTemplates::getAllBasics();

		$apiResponse = new APIResponse(APIResponse::STATUS_OK);
		$apiResponse->setData([
			'lists' => $lists
		]);
		return response()->json($apiResponse->getResponse(), APIResponse::CODE_OK);
	}

	/**
	 * Add a list
	 *
	 * @param $request Request
	 *
	 * @throws ValidationException In case the user's data is not valid or missing
	 *
	 * @return object Request
	 */
	public function add(Request $request)
	{
		try {
			$validator = Validator::make( $request->all(), [
				'listName' => 'required|string|min:1|max:96',
				'templateId' => 'required|integer'
			]);
			$this->checkForErrors($validator);

			// create a new list
			$list = new Lists();
			$list->user_id = $request->currentUser->id;
			$list->name = $request->input('listName');
			$list->save();
			$list->refresh();

			// manage template (adding the tasks, if need)
			$this->manageTemplate($list->id, $request->input('templateId'));

			// add the new tasks
			$newList = $this->prepareNewList($list);

			// add new tasks order
			$tasksOrder = new TasksOrder();
			$tasksOrder->list_id = $list->id;
			$tasksOrder->save();

			// setup the order
			$this->setupOrderAdd($list);

		} catch (ValidationException $ex) {
			return APIResponseFactory::makeValidationError($ex);
		}

		$apiResponse = new APIResponse(APIResponse::STATUS_OK);
		$apiResponse->setData([
			'list' => $newList
		]);
		return response()->json($apiResponse->getResponse(), APIResponse::CODE_OK);
	}

	/**
	 * Prepare the list data for response
	 *
	 * @param $listSource Lists An object that contains the database data
	 *
	 * @return array
	 */
	private function prepareNewList(Lists $listSource)
	{
		$list = [
			'id' => $listSource->id,
			'name' => $listSource->name,
			'hide_completed_items' => $listSource->hide_completed_items,
			'tasks' => Task::getListTasks($listSource->id)
		];

		return $list;
	}

	/**
	 * Delete a list
	 *
	 * @param integer $listId The id of the list to be deleted
	 *
	 * @throws InvalidOwnerException In case the task is not owned by the current user
	 *
	 * @return object Response
	 */
	public function deleteList(Request $request, $listId)
	{
		try {
			Lists::validateOwner($listId, $request->currentUser->id);

			$list = Lists::find($listId);
			$this->setupOrderDelete($list);
			$list->delete();

		} catch(InvalidOwnerException $ex) {
			return APIResponseFactory::makeInvalidOwner($ex);
		}

		$apiResponse = new APIResponse(APIResponse::STATUS_OK);
		$apiResponse->setData([
		]);
		return response()->json($apiResponse->getResponse(), APIResponse::CODE_OK);
	}

	/**
	 * Get user's Lists with their Tasks
	 *
	 * @param $request Request
	 *
	 * @return object Response
	 */
	public function getUserListsWithTasks(Request $request)
	{
		// Lists

		$listsOrder = ListsOrder::getByUserId($request->currentUser->id);

		if ( ! empty($listsOrder[0]->order)) {
			$lists = Lists::getUserListsOrdered($request->currentUser->id, $listsOrder[0]->order);
		}
		else {
			$lists = Lists::getUserLists($request->currentUser->id);
		}

		// Tasks

		// adds tasks for each list
		foreach ($lists as & $list) {

			if ( ! empty($list->tasks_order)) {
				$list->tasks = Task::getListTasksOrdered($list->id, $list->tasks_order);
			}
			else {
				$list->tasks = Task::getListTasks($list->id);
			}

			unset($list->tasks_order); // no need anymore
		}

		$apiResponse = new APIResponse(APIResponse::STATUS_OK);
		$apiResponse->setData([
			'lists' => $lists
		]);
		return response()->json($apiResponse->getResponse(), APIResponse::CODE_OK);
	}

	/**
	 * Toggle list visibility flag
	 *
	 * @param integer $listId The id of the list
	 *
	 * @throws InvalidOwnerException  In case the list is not owned by the current user
	 * @throws ModelNotFoundException In case the list does not exist
	 *
	 * @return object Response
	 */
	public function toggleCompletedTasksVisibility(Request $request, $listId)
	{
		try {
			Lists::validateOwner($listId, $request->currentUser->id);

			$list = Lists::find($listId);
			$list->hide_completed_items = ($list->hide_completed_items == 0) ? 1 : 0;
			$list->save();

		} catch (InvalidOwnerException $ex) {
			return APIResponseFactory::makeInvalidOwnerWithToken($ex);
		}

		$apiResponse = new APIResponse(APIResponse::STATUS_OK);
		$apiResponse->setData([
		]);
		return response()->json($apiResponse->getResponse(), APIResponse::CODE_OK);
	}

	private function manageTemplate($listId, $templateId)
	{
		// Blank
		if ($templateId == 0) {
			return true;
		}
		// Copy from user's list
		else if ($templateId > 0) {
			return Task::copyFromTo($templateId, $listId);
		}
		// Copy from task templates
		else {
			return TaskTemplates::copyFromTo(abs($templateId), $listId);
		}
	}

	private function setupOrderAdd(Lists $list)
	{
		$listsOrder = ListsOrder::where('user_id', $list->user_id)->first();
		$listsOrder->order = empty($listsOrder->order) ? $list->id : $list->id . $this->orderItemSeparator . $listsOrder->order;
		$listsOrder->save();
	}

	private function setupOrderDelete(Lists $list)
	{
		$listsOrder = ListsOrder::where('user_id', $list->user_id)->first();
		$listsOrder->order = $this->deleteOrderItem($listsOrder->order, $list->id);
		$listsOrder->save();
	}
}
