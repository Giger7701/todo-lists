<?php

namespace App\Http\Controllers;

use Validator;

use Illuminate\{
	Http\Request,
	Support\Facades\Log
};

use App\{
	APIResponse,
	APIResponseFactory
};

class InitController extends Controller
{
	/**
	 * Initial client request
	 *
	 * Response with 'OK' and include additional data, if need!
	 *
	 * @param Request $request
	 *
	 * @return object Response
	 */
	public function index(Request $request)
	{
		$apiResponse = new APIResponse(APIResponse::STATUS_OK);
		$apiResponse->setData([
			// additional data here
		]);
		return response()->json($apiResponse->getResponse(), APIResponse::CODE_OK);
	}
}
