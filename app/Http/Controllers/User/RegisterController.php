<?php

namespace App\Http\Controllers\User;

use Validator;

use Illuminate\{
	Http\Request,
	Support\Facades\Hash
};

use App\{
	User,
	UserVerification,
	Email,
	Traits\SimpleToken,
	Traits\Validations,
	Http\Controllers\Controller,
	Exceptions\RegisterException,
	Exceptions\ValidationException,
	APIResponse,
	APIResponseFactory,
	UserPhotos,
	ListsOrder
};

class RegisterController extends Controller
{
	use SimpleToken;
	use Validations;

	/**
	 * Verify a new user registration
	 *
	 * @param Request $request
	 *
	 * @throws RegisterException In case of invalid data supplied
	 *
	 * @return object Response
	 */
	public function verifyAccount(Request $request)
	{
		try {
			$validator = Validator::make( $request->all(), [
				'token' => 'required|string|regex:/^[a-fA-F0-9]{' . self::$tokenLength . '}$/'
			]);
			$this->checkForErrors($validator);

			// verify token
			$userVerification = UserVerification::where('token', $request->input('token'))->first();
			if (empty($userVerification)) {
				throw new RegisterException('invalid_verification_token');
			}
			$user_id = $userVerification->user_id;
			$userVerification->delete();

			// verify user account
			$user = User::find($user_id);
			if (empty($user)) {
				throw new RegisterException('user_not_found');
			}
			$user->verified = 1;
			$user->save();

		}
		catch (ValidationException $ex) {
			return APIResponseFactory::makeValidationError($ex);
		}
		catch (RegisterException $ex) {
			return APIResponseFactory::makeValidationError($ex);
		}

		return response()->json((new APIResponse(APIResponse::STATUS_OK))->getResponse(), APIResponse::CODE_OK);
	}

	/**
	 * Register new regular user
	 *
	 * @param Request $request
	 *
	 * @throws RegisterException In case of invalid data supplied
	 *
	 * @return object Response
	 */
	public function index(Request $request)
	{
		try {
			$validator = Validator::make( $request->all(), [
				'username' => 'required|string|max:30|min:3|unique:users,name',
				'password' => 'required|string|max:30|min:' . User::PASSWORD_MIN_LENGTH,
				'repeat_password' => 'required|string|max:30|min:' . User::PASSWORD_MIN_LENGTH,
				'email' => 'required|email|unique:users,email'
			]);
			$this->checkForErrors($validator);

			if ($request->input('password') !== $request->input('repeat_password')) {
				throw new RegisterException('both_passwords_donot_match');
			}

			// add the user to the database
			$user = new User();
			$user->name = $request->input('username');
			$user->email = $request->input('email');
			$user->password = Hash::make($request->input('password'));
			$user->save();

			// add a record for this user in the 'user_photos' table
			$userPhotos = new UserPhotos();
			$userPhotos->user_id = $user->id;
			$userPhotos->save();

			// add a record for this user in the 'lists_order' table
			$listsOrder = new ListsOrder();
			$listsOrder->user_id = $user->id;
			$listsOrder->save();

			// add a new user verification record
			$userVerification = new UserVerification();
			$userVerification->user_id = $user->id;
			$userVerification->token = UserVerification::generateToken($user->id, $user->email);
			$userVerification->save();

			// send verification e-mail
			(new Email)->sendUserRegistration($userVerification->token, $user->email);

		}
		catch (ValidationException $ex) {
			return APIResponseFactory::makeValidationError($ex);
		}
		catch (RegisterException $ex) {
			return APIResponseFactory::makeValidationError($ex);
		}

		return response()->json((new APIResponse(APIResponse::STATUS_OK))->getResponse(), APIResponse::CODE_OK);
	}
}
