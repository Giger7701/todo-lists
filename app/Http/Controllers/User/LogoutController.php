<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;

use App\{
	APIResponse,
	Http\Controllers\Controller
};

class LogoutController extends Controller
{
	/**
	 * Logout the user from the system
	 *
	 * @param Request $request
	 *
	 * @return object Response
	 */
	public function index(Request $request)
	{
		return response()->json((new APIResponse(APIResponse::STATUS_OK))->getResponse(), APIResponse::CODE_OK);
	}
}
