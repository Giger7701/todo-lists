<?php

namespace App\Http\Controllers\User;

use Validator;

use Illuminate\{
	Http\Request,
	Support\Facades\Hash,
	Support\Facades\Log,
	Database\Eloquent\ModelNotFoundException
};

use App\{
	User,
	UserPasswordReset,
	APIResponse,
	APIResponseFactory,
	Traits\SimpleToken,
	Traits\Validations,
	Exceptions\ResetPasswordException,
	Exceptions\ValidationException,
	Http\Controllers\Controller
};

class ResetPasswordController extends Controller
{
	use SimpleToken;
	use Validations;

	/**
	 * Change the user's password
	 *
	 * @throws ResetPasswordException In case of passwords error
	 * @throws ModelNotFoundException In case of user cannot be found
	 *
	 * @return void
	 */
	public function index(Request $request)
	{
		try {
			list($password, $token) = $this->validateRequest($request);

			$userPasswordReset = UserPasswordReset::where('token', $token)->first();
			if (empty($userPasswordReset)) {
				throw new ResetPasswordException('invalid_expired_reset_pass_token');
			}
			$userPasswordReset->delete();

			$user = User::findOrFail($userPasswordReset->user_id);
			$user->password = $password;
			$user->save();

		}
		catch (ValidationException $ex) {
			return APIResponseFactory::makeValidationError($ex);
		}
		catch (ResetPasswordException $ex) {
			return APIResponseFactory::makeValidationError($ex);
		}
		catch (ModelNotFoundException $ex) {
			return APIResponseFactory::makeUserNotFound($ex);
		}

		return response()->json((new APIResponse(APIResponse::STATUS_OK))->getResponse(), APIResponse::CODE_OK);
	}

	/**
	 * Validates the password and token provided by the user
	 *
	 * @param $request Request
	 *
	 * @throws ResetPasswordException In case of password and/or token
	 *
	 * @return string The new password hashed
	 */
	private function validateRequest(Request $request)
	{
		$validator = Validator::make( $request->all(), [
			'password' => 'required|string|max:32|min:' . User::PASSWORD_MIN_LENGTH,
			'token' => 'required|string|regex:/^[a-fA-F0-9]{' . self::$tokenLength . '}$/'
		]);
		$this->checkForErrors($validator);

		return [
			Hash::make($request->input('password')),
			$request->input('token')
		];
	}
}
