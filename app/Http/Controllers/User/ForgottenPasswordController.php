<?php

namespace App\Http\Controllers\User;

use Validator;

use Illuminate\{
	Http\Request,
	Support\Facades\Log,
	Database\Eloquent\ModelNotFoundException
};

use App\{
	User,
	Email,
	UserPasswordReset,
	APIResponse,
	APIResponseFactory,
	Traits\SimpleToken,
	Exceptions\ForgottenPasswordException,
	Http\Controllers\Controller
};

class ForgottenPasswordController extends Controller
{
	use SimpleToken;

	/**
	 * Forgotten Password
	 *
	 * @param Request $request
	 *
	 * @throws UserProfileException In case of missing or invalid email address
	 *
	 * @return object Response
	 */
	public function index(Request $request)
	{
		try {
			$validator = Validator::make( $request->all(), [
				'email' => 'required|email|exists:users,email'
			]);
			$errors = $validator->errors();
			if ($errors->has('email')) {
				throw new ForgottenPasswordException('invalid_email');
			}

			$user = User::where('email', $request->input('email'))->first();
			if ($user->verified == 0) {
				throw new ForgottenPasswordException('invalid_email');
			}
			$userPasswordReset = UserPasswordReset::where('user_id', $user->id)->first();
			if (empty($userPasswordReset)) {
				$userPasswordReset = new UserPasswordReset();
				$userPasswordReset->user_id = $user->id;
			}
			$userPasswordReset->token = self::generateToken($user->id, $user->email);
			$userPasswordReset->save();

			// send reset password e-mail
			(new Email)->sendUserResetPassword($userPasswordReset->token, $user->email);

		} catch (ForgottenPasswordException $ex) {
			return APIResponseFactory::makeValidationError($ex);
		}

		return response()->json((new APIResponse(APIResponse::STATUS_OK))->getResponse(), APIResponse::CODE_OK);
	}
}
