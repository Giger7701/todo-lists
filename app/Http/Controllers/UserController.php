<?php

namespace App\Http\Controllers;

use Validator;

use Illuminate\{
	Http\Request,
	Database\Eloquent\ModelNotFoundException,
	Support\Facades\Log,
	Support\Facades\Hash
};

use App\{
	APIResponse,
	APIResponseFactory,
	User,
	Traits\Validations,
	Exceptions\UserProfileException,
	Exceptions\UserRegistrationException,
	Exceptions\ValidationException,
	UserPhotosManager,
	UserPhotos
};

class UserController extends Controller
{
	use Validations;

	/**
	 * Update user's profile photo
	 *
	 * @param Request $request
	 *
	 * @throws ModelNotFoundException In case of user cannot be found
	 * @throws UserProfileException   In case of missing profile image file
	 *
	 * @return object Response
	 */
	public function updateProfilePhoto(Request $request)
	{
		try {
			if ($request->hasFile('userProfileImage')) {
				$userProfileImage = $request->file('userProfileImage');
				list($photo_big, $photo_small) = $this->setupProfileImageFile($userProfileImage, $request->currentUser->id);
			}
			else {
				throw new UserProfileException('Missing image file');
			}

		} catch (UserProfileException $ex) {
			return APIResponseFactory::makeUserProfileNotFound($ex);
		}
		catch (ModelNotFoundException $ex) {
			return APIResponseFactory::makeUserNotFound($ex);
		}

		$data = [
			'userName' => ucfirst(strtolower($request->currentUser->name))
		];
		$this->addProfilePhotoData($data, [
			'big' => $photo_big,
			'small' => $photo_small
		]);
		$apiResponse = new APIResponse(APIResponse::STATUS_OK);
		$apiResponse->setData($data);
		return response()->json($apiResponse->getResponse(), APIResponse::CODE_OK);
	}

	/**
	 * Delete user's profile photo
	 *
	 * @param Request $request
	 *
	 * @throws ModelNotFoundException In case of user cannot be found
	 *
	 * @return object Response
	 */
	public function deleteProfilePhoto(Request $request)
	{
		$photosObject = UserPhotos::where('user_id', $request->currentUser->id);
		$photos = $photosObject->get();
		if (count($photos) > 0) {
			(new UserPhotosManager())->deleteProfilePhotos([
				'photo_big' => $photos[0]->photo_big,
				'photo_small' => $photos[0]->photo_small
			]);
			$photosObject->delete();
		}

		$response = new APIResponse(APIResponse::STATUS_OK);
		$response->setData([
		]);
		return response()->json($response->getResponse(), APIResponse::CODE_OK);
	}

	/**
	 * Get user's profile info
	 *
	 * @param Request $request
	 *
	 * @throws ModelNotFoundException In case of user cannot be found
	 *
	 * @return object Response
	 */
	public function getProfileData(Request $request)
	{
		$data = [
			'username' => ucfirst(strtolower($request->currentUser->name))
		];
		$this->addProfilePhotoData($data, $request->currentUser->getPhotos());

		$response = new APIResponse(APIResponse::STATUS_OK);
		$response->setData($data);
		return response()->json($response->getResponse(), APIResponse::CODE_OK);
	}

	/**
	 * Change the user's password
	 *
	 * @param Request $request
	 *
	 * @throws UserProfileException   In case of passwords error
	 * @throws ModelNotFoundException In case of user cannot be found
	 *
	 * @return void
	 */
	public function changePassword(Request $request)
	{
		try {
			$request->currentUser->password = $this->validatePassword($request, $request->currentUser->password);
			$request->currentUser->save();

		}
		catch (ValidationException $ex) {
			return APIResponseFactory::makeValidationError($ex);
		}
		catch (UserProfileException $ex) {
			return APIResponseFactory::makeValidationError($ex);
		}

		$response = new APIResponse(APIResponse::STATUS_OK);
		$response->setData([]);
		return response()->json($response->getResponse(), APIResponse::CODE_OK);
	}

	/**
	 * Validates the passwords provided by the user
	 *
	 * @param Request $request
	 *
	 * @throws UserProfileException In case of passwords error
	 *
	 * @return string The new password hashed
	 */
	private function validatePassword(Request $request, $currentPassword)
	{
		$validator = Validator::make( $request->all(), [
			'currentPassword' => 'required|string|max:32|min:' . User::PASSWORD_MIN_LENGTH,
			'newPassword' => 'required|string|max:32|min:'     . User::PASSWORD_MIN_LENGTH,
			'repeatPassword' => 'required|string|max:32|min:'  . User::PASSWORD_MIN_LENGTH
		]);
		$this->checkForErrors($validator);

		if ( ! Hash::check($request->input('currentPassword'), $currentPassword)) {
			throw new UserProfileException('invalid_current_password');
		}

		if ($request->input('newPassword') !== $request->input('newPassword')) {
			throw new UserProfileException('both_passwords_donot_match');
		}

		return Hash::make($request->input('newPassword'));
	}

	/**
	 * Add a user's profile photo set to the response object
	 *
	 * @return void
	 */
	private function addProfilePhotoData( & $data, $photos)
	{
		if (is_array($photos)) {
			$data['userProfileImage'] = $photos;
		}
	}

	/**
	 * Validates the passwords provided by the user
	 *
	 * @param object $file The file uploaded by the user
	 * @param integer $userId The user id
	 *
	 * @return array The new profile image files for "big" and "small" photos
	 */
	private function setupProfileImageFile($file, $userId)
	{
		$userPhotosManager = new UserPhotosManager();
		list($photo_big, $photo_small) = $userPhotosManager->setupNew(
			$file->path(),
			$file->getClientMimeType(),
			$file->getClientSize(),
			$file->clientExtension(),
			$userId
		);

		$userPhotos = UserPhotos::where('user_id', $userId)->first();
		if ( ! empty($userPhotos)) {
			$userPhotosManager->deleteProfilePhotos([
				'photo_big' => $userPhotos->photo_big,
				'photo_small' => $userPhotos->photo_small
			]);
		}
		else {
			$userPhotos = new UserPhotos();
			$userPhotos->user_id  = $userId;
		}

		$userPhotos->photo_big = $photo_big;
		$userPhotos->photo_small = $photo_small;
		$userPhotos->save();

		return [
			$photo_big,
			$photo_small
		];

		/*
		$file->getClientOriginalExtension()
		$file->clientExtension()

		$file->getPathname()
		$file->getClientOriginalName()
		$file->getError()
		*/
	}
}
