<?php

namespace App\Http\Controllers;

use Validator;

use Illuminate\{
	Http\Request,
	Support\Facades\Log
};

use App\{
	APIResponse,
	APIResponseFactory,
	Exceptions\InvalidOwnerException,
	Exceptions\ValidationException,
	Task,
	Lists,
	TasksOrder,
	Traits\OrderItems,
	Traits\Validations
};

class TasksController extends Controller
{
	use OrderItems;
	use Validations;

	/**
	 * Update list's task order
	 *
	 * @param $request Request
	 *
	 * @return object Response
	 */
	public function updateOrder(Request $request)
	{
		try {
			$validator = Validator::make( $request->all(), [
				'listId' => 'required|integer|min:1',
				'tasksOrder' => 'required|regex:' . $this->orderRegEx
			]);
			$this->checkForErrors($validator);

			Lists::validateOwner($request->input('listId'), $request->currentUser->id);

			$tasksOrder = TasksOrder::where('list_id', $request->input('listId'))->first();
			$tasksOrder->order = $request->input('tasksOrder');
			$tasksOrder->save();

		} catch (ValidationException $ex) {
			return APIResponseFactory::makeValidationError($ex);
		} catch (InvalidOwnerException $ex) {
			return APIResponseFactory::makeInvalidOwner($ex);
		}

		$apiResponse = new APIResponse(APIResponse::STATUS_OK);
		$apiResponse->setData([
		]);
		return response()->json($apiResponse->getResponse(), APIResponse::CODE_OK);
	}

	/**
	 * Rename a task
	 *
	 * @param $request Request
	 *
	 * @throws ValidationException   In the required information received by the user is invalid/missing
	 * @throws InvalidOwnerException In case the taks is not owned by the current user
	 *
	 * @return object Response
	 */
	public function rename(Request $request)
	{
		try {
			$validator = Validator::make( $request->all(), [
				'taskId' => 'required|integer|min:1',
				'taskName' => 'required|string|min:1|max:128'
			]);
			$this->checkForErrors($validator);

			Task::validateOwner($request->input('taskId'), $request->currentUser->id);

			$task = Task::find($request->input('taskId'));
			$task->name = $request->input('taskName');
			$task->save();

		} catch (ValidationException $ex) {
			return APIResponseFactory::makeValidationError($ex);
		} catch (InvalidOwnerException $ex) {
			return APIResponseFactory::makeInvalidOwner($ex);
		}

		$apiResponse = new APIResponse(APIResponse::STATUS_OK);
		$apiResponse->setData([
		]);
		return response()->json($apiResponse->getResponse(), APIResponse::CODE_OK);
	}

	/**
	 * Delete a task
	 *
	 * @param $id The id of the task
	 *
	 * @throws InvalidOwnerException In case the taks is not owned by the current user
	 *
	 * @return object Response
	 */
	public function delete(Request $request, $id)
	{
		try {
			Task::validateOwner($id, $request->currentUser->id);

			$task = Task::find($id);
			$this->setupOrderDelete($task);
			$task->delete();

		} catch (InvalidOwnerException $ex) {
			return APIResponseFactory::makeInvalidOwner($ex);
		}

		$apiResponse = new APIResponse(APIResponse::STATUS_OK);
		$apiResponse->setData([
		]);
		return response()->json($apiResponse->getResponse(), APIResponse::CODE_OK);
	}

	/**
	 * Mark a task as completed
	 *
	 * @param $id The id of the task
	 *
	 * @throws InvalidOwnerException In case the taks is not owned by the current user
	 *
	 * @return object Response
	 */
	public function complete(Request $request)
	{
		try {
			$validator = Validator::make( $request->all(), [
				'taskId' => 'required|integer|min:1'
			]);
			$this->checkForErrors($validator);

			Task::validateOwner($request->input('taskId'), $request->currentUser->id);

			$task = Task::find($request->input('taskId'));
			$task->completed = 1;
			$task->save();

		} catch (ValidationException $ex) {
			return APIResponseFactory::makeValidationError($ex);
		} catch (InvalidOwnerException $ex) {
			return APIResponseFactory::makeInvalidOwner($ex);
		}

		$apiResponse = new APIResponse(APIResponse::STATUS_OK);
		$apiResponse->setData([
		]);
		return response()->json($apiResponse->getResponse(), APIResponse::CODE_OK);
	}

	/**
	 * Add a new task
	 *
	 * @param $id The id of the task
	 *
	 * @throws InvalidOwnerException In case the taks is not owned by the current user
	 *
	 * @return object Response
	 */
	public function add(Request $request)
	{
		try {
			$validator = Validator::make( $request->all(), [
				'listId' => 'required|integer|min:1',
				'taskName' => 'required|string|min:1|max:128'
			]);
			$this->checkForErrors($validator);

			// validate the owner of the list we are trying to modfy
			$list = new Lists();
			$list->validateOwner($request->input('listId'), $request->currentUser->id);

		} catch(ValidationException $ex) {
			return APIResponseFactory::makeValidationError($ex);
		} catch (InvalidOwnerException $ex) {
			return APIResponseFactory::makeInvalidOwner($ex);
		}

		$task = new Task();
		$task->list_id = $request->input('listId');
		$task->name = $request->input('taskName');
		$task->save();    // the column "completed" is missing here, this might happens bacause the column has a default value
		$task->refresh(); // refresh to get the missing one

		// setup the order
		$this->setupOrderAdd($task);

		$apiResponse = new APIResponse(APIResponse::STATUS_OK);
		$apiResponse->setData([
			'id' => $task->id,
			'name' => $task->name,
			'completed' => $task->completed
		]);
		return response()->json($apiResponse->getResponse(), 200); // 201
	}

	private function setupOrderAdd(Task $task)
	{
		$tasksOrder = TasksOrder::where('list_id', $task->list_id)->first();
		$tasksOrder->order = empty($tasksOrder->order) ? $task->id : $task->id . $this->orderItemSeparator . $tasksOrder->order;
		$tasksOrder->save();
	}

	private function setupOrderDelete(Task $task)
	{
		$tasksOrder = TasksOrder::where('list_id', $task->list_id)->first();
		$tasksOrder->order = $this->deleteOrderItem($tasksOrder->order, $task->id);
		$tasksOrder->save();
	}
}
