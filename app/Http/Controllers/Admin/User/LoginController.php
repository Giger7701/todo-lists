<?php

namespace App\Http\Controllers\Admin\User;

use Validator;

use Illuminate\{
	Database\Eloquent\ModelNotFoundException,
	Http\Request,
	Support\Facades\Log,
	Support\Facades\Hash
};

use JWTAuth;
use JWTFactory;

use App\{
	User,
	Http\Controllers\Controller,
	Exceptions\LoginException,
	Exceptions\ValidationException,
	APIResponse,
	APIResponseFactory,
	Traits\Validations
};

class LoginController extends Controller
{
	use Validations;

	/**
	 * User login request
	 *
	 * @param Request $request
	 *
	 * @throws LoginException         In case of invalid credentials supplied
	 * @throws ValidationException    In case of invalid data received from the client
	 * @throws ModelNotFoundException In case of user cannot be found
	 *
	 * @return object Response
	 */
	public function index(Request $request)
	{
		try {
			$validator = Validator::make( $request->all(), [
				'username' => 'required|string|max:30|min:3|exists:users,name',
				'password' => 'required|string|max:30|min:' . User::PASSWORD_MIN_LENGTH
			]);
			$this->checkForErrors($validator);

			$user = User::where([
				'type' => 'admin',
				'name' => $request->input('username')
			])->firstOrFail();

			if (Hash::check($request->input('password'), $user->password)) {
				$payload = JWTFactory::sub($user->id)->make();
				$token = JWTAuth::encode($payload)->get();
			}
			else {
				throw new LoginException('invalid_credentials');
			}
		}
		catch (ValidationException $ex) {
			return APIResponseFactory::makeValidationError($ex);
		}
		catch (LoginException $ex) {
			return APIResponseFactory::makeValidationError($ex);
		}
		catch (ModelNotFoundException $ex) {
			return APIResponseFactory::makeUserNotFound($ex);
		}

		$apiResponse = new APIResponse(APIResponse::STATUS_OK);
		$apiResponse->setData([
			'token' => $token
		]);
		return response()->json($apiResponse->getResponse(), APIResponse::CODE_OK);
	}
}
