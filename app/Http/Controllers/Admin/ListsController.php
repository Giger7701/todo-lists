<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\{
	Http\Request,
	Support\Facades\Log,
	Support\Facades\DB
};

use App\{
	User,
	Lists,
	Task,
	Pagination,
	APIResponse,
	APIResponseFactory,
	Http\Controllers\Controller,
	Exceptions\ValidationException,
	Traits\Validations
};

class ListsController extends Controller
{
	use Validations;

	/**
	 * Delete a list of lists from the database
	 *
	 * @param Request $request The request object
	 * @param integer $listId  List id
	 *
	 * @throws ValidationException Invalid data received from the client
	 *
	 * @return response Response
	 */
	public function fetchTasks(Request $request, $listId)
	{
		$tasks = Task::where('list_id', $listId)->get();

		$data = [];
		foreach ($tasks as $task) {
			$data[] = $task->toArray();
		}

		$apiResponse = new APIResponse(APIResponse::STATUS_OK);
		$apiResponse->setData($data);
		return response()->json($apiResponse->getResponse(), APIResponse::CODE_OK);
	}

	/**
	 * Delete a list of lists from the database
	 *
	 * @param Request $request
	 *
	 * @throws ValidationException Invalid data received from the client
	 *
	 * @return response Response
	 */
	public function delete(Request $request)
	{
		try {
			$validator = Validator::make( $request->all(), [
				'lists' => 'required|array',
				'lists.*' => 'required|integer|min:1'
			]);
			$this->checkForErrors($validator);

			// delete the users
			$user = Lists::whereIn('id', $request->input('lists'))->delete();
		}
		catch (ValidationException $ex) {
			return APIResponseFactory::makeValidationError($ex);
		}

		return response()->json((new APIResponse(APIResponse::STATUS_OK))->getResponse(), APIResponse::CODE_OK);
	}

	/**
	 * Fetch all users from the database, (id, name)
	 *
	 * @param Request $request
	 *
	 * @throws ValidationException Invalid data received from the client
	 *
	 * @return response Response
	 */
	public function fetchUsers(Request $request)
	{
		$users = User::where('type', 'user')->get();

		$data = [];
		foreach ($users as $user) {
			$data[] = $user->toArray();
		}

		$apiResponse = new APIResponse(APIResponse::STATUS_OK);
		$apiResponse->setData($data);
		return response()->json($apiResponse->getResponse(), APIResponse::CODE_OK);
	}

	/**
	 * Fetch all user's lists from the database, including the number of tasks they have
	 *
	 * @param Request $request
	 *
	 * @throws ValidationException Invalid data received from the client
	 *
	 * @return response Response
	 */
	public function fetch(Request $request)
	{
		try {
			$validator = Validator::make( $request->all(), [
				'sortColumn' => array('string', 'regex:/^id|user_name|name|created_at|total_tasks$/'),
				'sortDirection' => array('string', 'regex:/^asc|desc$/'),
				'searchQuery' => 'string|min:1',
				'filterByUserId' => array('string', 'regex:/^(?!0)\d+$/'),
				'itemsPerPage' => 'required|integer|min:1',
				'pageNumber' => 'required|integer|min:1'
			]);
			$this->checkForErrors($validator);
		}
		catch (ValidationException $ex) {
			return APIResponseFactory::makeValidationError($ex);
		}

		$data = [];
		list($data['page'], $data['total_lists'], $data['lists']) = $this->fetchListsData(
			$request->input('itemsPerPage'),
			$request->input('pageNumber'),
			$request->input('searchQuery'),
			$request->input('filterByUserId'),
			$request->input('sortColumn'),
			$request->input('sortDirection')
		);

		$apiResponse = new APIResponse(APIResponse::STATUS_OK);
		$apiResponse->setData($data);
		return response()->json($apiResponse->getResponse(), APIResponse::CODE_OK);
	}

	private function fetchListsData($itemsPerPage, $pageNumber, $searchQuery, $filterByUserId, $sortColumn, $sortDirection)
	{
		// additional where
		$additionalWhere = '';
		$this->compileAdditionalWhere($additionalWhere, $searchQuery, $filterByUserId);

		// compile order by clause
		$orderBy = '';
		$this->compileOrderBy($orderBy, $sortColumn, $sortDirection);

		// get total lists
		$total_lists = Lists::getTotalListsFiltered($additionalWhere);
		if ($total_lists == 0) {
			return [1, 0, []];
		}

		// pagination data
		list($pageNumber, $startFrom, $maxItems) = Pagination::getDBConditions($total_lists, $itemsPerPage, $pageNumber);

		// db query
		$result = Lists::getLimitedWithTasksCounted($additionalWhere, $orderBy, $startFrom, $maxItems);

		// prepare data
		// this is needed because the database query returns an array of objects
		$data = [];
		foreach ($result as $item) {
			$data[] = [
				'id' => $item->id,
				'name' => $item->name,
				'created_at' => $item->created_at,
				'total_tasks' => $item->total_tasks,
				'user_name' => $item->user_name
			];
		}
		if (empty($data)) {
			return [1, 0, []];
		}

		return [$pageNumber, $total_lists, $data];
	}

	private function compileAdditionalWhere( & $additionalWhere, $searchQuery, $filterByUserId)
	{
		if ( ! empty($searchQuery)) {
			$additionalWhere .= 'AND l.name LIKE "%' . $searchQuery . '%"';
		}

		if ( (int) $filterByUserId > 0) {
			$additionalWhere .= ' AND u.id = ' . $filterByUserId;
		}
	}

	private function compileOrderBy( & $orderBy, $sortColumn, $sortDirection)
	{
		if ($sortColumn == 'user_name') {
			$sortColumn = 'u.name';
		}
		else if ($sortColumn != 'total_tasks') {
			$sortColumn = 'l.' . $sortColumn;
		}

		$orderBy .= $sortColumn . ' ' . strtoupper($sortDirection);
	}
}
