<?php

namespace App\Http\Controllers\Admin;

use Illuminate\{
	Http\Request,
	Support\Facades\Log,
	Support\Facades\DB
};

use App\{
	APIResponse,
	APIResponseFactory,
	Http\Controllers\Controller
};

class InitController extends Controller
{
	/**
	 * Initial client request
	 *
	 * Response with 'OK', as well include additional data (if need)!
	 *
	 * @param Request $request
	 *
	 * @return object Response
	 */
	public function index(Request $request)
	{
		$apiResponse = new APIResponse(APIResponse::STATUS_OK);
		$apiResponse->setData([
			// additional data here
		]);
		return response()->json($apiResponse->getResponse(), APIResponse::CODE_OK);
	}
}
