<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\{
	Http\Request,
	Support\Facades\Log,
	Support\Facades\DB,
	Database\Eloquent\ModelNotFoundException
};

use JWTAuth;
use JWTFactory;

use App\{
	User,
	Pagination,
	APIResponse,
	APIResponseFactory,
	Http\Controllers\Controller,
	Exceptions\ValidationException,
	Traits\Validations
};

class UsersController extends Controller
{
	use Validations;

	/**
	 * Add additional login info to the token
	 *
	 * @param Request $request
	 *
	 * @throws ModelNotFoundException  In case of user cannot be found
	 *
	 * @return response Response
	 */
	public function loginAsThisUser(Request $request, $userId)
	{
		try {
			// generate new token with the new user id
			// $payload = JWTFactory::sub($request->currentUser->id)->aud('loggedInAs')->loggedInAs(['userId' => $userId])->make();
			$user = User::findOrFail($userId);
			$payload = JWTFactory::sub($userId)->make();
			$token = JWTAuth::encode($payload)->get();
		}
		catch (ModelNotFoundException $ex) {
			return APIResponseFactory::makeValidationError($ex);
		}

		$apiResponse = new APIResponse(APIResponse::STATUS_OK);
		$apiResponse->setData([
			'token' => $token
		]);
		return response()->json($apiResponse->getResponse(), APIResponse::CODE_OK);
	}

	/**
	 * Delete a list of users from the database
	 *
	 * @param Request $request
	 *
	 * @throws ValidationException Invalid data received from the client
	 *
	 * @return response Response
	 */
	public function delete(Request $request)
	{
		try {
			$validator = Validator::make( $request->all(), [
				'users' => 'required|array',
				'users.*' => 'required|integer|min:1'
			]);
			$this->checkForErrors($validator);

			// delete the users
			$user = User::where('type', 'user')->whereIn('id', $request->input('users'))->delete();
		}
		catch (ValidationException $ex) {
			return APIResponseFactory::makeValidationError($ex);
		}

		return response()->json((new APIResponse(APIResponse::STATUS_OK))->getResponse(), APIResponse::CODE_OK);
	}

	/**
	 * Toggle user account(s). Set "active" user flag to "0" or "1".
	 *
	 * @param Request $request
	 *
	 * @throws ValidationException Invalid data received from the client
	 *
	 * @return response Response
	 */
	public function toggleAccounts(Request $request)
	{
		try {
			$validator = Validator::make( $request->all(), [
				'users' => 'required|array',
				'users.*' => 'required|integer|min:1'
			]);
			$this->checkForErrors($validator);

			// update the users flag "active"
			User::where('type', 'user')->whereIn('id', $request->input('users'))->update(['active' => $request->route()->getAction()['flagValue']]);
		}
		catch (ValidationException $ex) {
			return APIResponseFactory::makeValidationError($ex);
		}

		return response()->json((new APIResponse(APIResponse::STATUS_OK))->getResponse(), APIResponse::CODE_OK);
	}

	/**
	 * Fetch all users from the database, including the number of lists they own
	 *
	 * @param Request $request
	 *
	 * @throws ValidationException Invalid data received from the client
	 *
	 * @return response Response
	 */
	public function fetch(Request $request)
	{
		try {
			$validator = Validator::make( $request->all(), [
				'sortColumn' => array('string', 'regex:/^id|name|email|created_at|updated_at|active|total_lists$/'),
				'sortDirection' => array('string', 'regex:/^asc|desc$/'),
				'searchQuery' => 'string|min:1',
				'filterByState' => array('string', 'regex:/^all|active|inactive$/'),
				'itemsPerPage' => 'required|integer|min:1',
				'pageNumber' => 'required|integer|min:1'
			]);
			$this->checkForErrors($validator);
		}
		catch (ValidationException $ex) {
			return APIResponseFactory::makeValidationError($ex);
		}

		$data = [];
		list($data['page'], $data['total_users'], $data['users']) = $this->fetchUsersData(
			$request->input('itemsPerPage'),
			$request->input('pageNumber'),
			$request->input('searchQuery'),
			$request->input('filterByState'),
			$request->input('sortColumn'),
			$request->input('sortDirection')
		);

		$apiResponse = new APIResponse(APIResponse::STATUS_OK);
		$apiResponse->setData($data);
		return response()->json($apiResponse->getResponse(), APIResponse::CODE_OK);
	}

	private function fetchUsersData($itemsPerPage, $pageNumber, $searchQuery, $filterByState, $sortColumn, $sortDirection)
	{
		// additional where
		$additionalWhere = '';
		$this->compileAdditionalWhere($additionalWhere, $searchQuery, $filterByState);

		// compile order by clause
		$orderBy = '';
		$this->compileOrderBy($orderBy, $sortColumn, $sortDirection);

		// get total users
		$total_users = User::getTotalUsersFiltered($additionalWhere);
		if ($total_users == 0) {
			return [1, 0, []];
		}

		// pagination data
		list($pageNumber, $startFrom, $maxItems) = Pagination::getDBConditions($total_users, $itemsPerPage, $pageNumber);

		// db query
		$result = User::getLimitedWithListsCounted($additionalWhere, $orderBy, $startFrom, $maxItems);

		// prepare data
		$data = [];
		foreach ($result as $user) {
			$data[] = [
				'id' => $user->id,
				'name' => $user->name,
				'email' => $user->email,
				'active' => $user->active,
				'created_at' => $user->created_at,
				'updated_at' => $user->updated_at,
				'total_lists' => $user->total_lists
			];
		}
		if (empty($data)) {
			return [1, 0, []];
		}

		return [$pageNumber, $total_users, $data];
	}

	private function compileOrderBy( & $orderBy, $sortColumn, $sortDirection)
	{
		if ($sortColumn != 'total_lists') {
			$sortColumn = 'u.' . $sortColumn;
		}

		$orderBy .= $sortColumn . ' ' . strtoupper($sortDirection);
	}

	private function compileAdditionalWhere( & $additionalWhere, $searchQuery, $filterByState)
	{
		if ( ! empty($searchQuery)) {
			$additionalWhere .= 'AND u.name LIKE "%' . $searchQuery . '%"';
		}

		switch ($filterByState) {
			// case 'all':
			//	break;
			case 'active':
				$additionalWhere .= ' AND u.active = 1';
				break;
			case 'inactive':
				$additionalWhere .= ' AND u.active = 0';
				break;
		}
	}
}
