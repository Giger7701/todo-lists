<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\{
	// Validation\ValidationException, // this one is bugged
	Http\Request,
	Support\Facades\Log,
	Support\Facades\DB
};

use App\{
	Email,
	APIResponse,
	APIResponseFactory,
	Http\Controllers\Controller,
	Exceptions\ValidationException,
	Traits\Validations
};

class DashboardController extends Controller
{
	use Validations;

	/**
	 * Send e-mails to all users
	 *
	 * @param Request $request
	 *
	 * @return response Response
	 */
	public function massEmailing(Request $request)
	{
		try {
			$validator = Validator::make( $request->all(), [
				'from' => 'required|string|min:1',
				'subject' => 'required|string|min:1',
				'body' => 'required|string|min:1'
			]);
			$this->checkForErrors($validator);

			// mass e-mail sending
			(new Email())->sendToAllUsers(
				$request->input('from'),
				$request->input('subject'),
				$request->input('body')
			);
		}
		catch (ValidationException $ex) {
			return APIResponseFactory::makeValidationError($ex);
		}

		return response()->json((new APIResponse(APIResponse::STATUS_OK))->getResponse(), APIResponse::CODE_OK);
	}

	/**
	 * Get mass e-mails history statistics
	 *
	 * @param Request $request
	 *
	 * @return response Response
	 */
	public function fetchEmailsHistory(Request $request)
	{
		$data = $this->getEmailsHistoryData();

		$apiResponse = new APIResponse(APIResponse::STATUS_OK);
		$apiResponse->setData($data);
		return response()->json($apiResponse->getResponse(), APIResponse::CODE_OK);
	}

	/**
	 * Get bsic usage statistics
	 *
	 * @param Request $request
	 *
	 * @return response Response
	 */
	public function fetchStatistics(Request $request)
	{
		$data =  $this->getStatisticsData();

		$apiResponse = new APIResponse(APIResponse::STATUS_OK);
		$apiResponse->setData($data);
		return response()->json($apiResponse->getResponse(), APIResponse::CODE_OK);
	}

	private function getStatisticsData()
	{
		$result = DB::select('
			SELECT
				(SELECT COUNT(id) FROM users WHERE type = "user") AS total_users,
				(SELECT COUNT(l.id) FROM lists AS l JOIN users AS u ON (u.id = l.user_id) WHERE u.type = "user") AS total_lists,
				(SELECT COUNT(t.id) FROM tasks AS t JOIN lists AS l ON (l.id = t.list_id) JOIN users AS u ON (u.id = l.user_id) WHERE u.type = "user" AND t.completed = 0) AS tasks_opened,
				(SELECT COUNT(t.id) FROM tasks AS t JOIN lists AS l ON (l.id = t.list_id) JOIN users AS u ON (u.id = l.user_id) WHERE u.type = "user" AND t.completed = 1) AS tasks_closed,
				(SELECT COUNT(l.id) FROM lists AS l JOIN users AS u ON (u.id = l.user_id) WHERE u.type = "user" AND DATE(l.created_at) = DATE(DATE_SUB(CURRENT_TIMESTAMP, INTERVAL 1 DAY))) AS lists_created_yesterday,
				(SELECT COUNT(t.id) FROM tasks AS t JOIN lists AS l ON (l.id = t.list_id) JOIN users AS u ON (u.id = l.user_id) WHERE u.type = "user" AND DATE(l.created_at) = DATE(DATE_SUB(CURRENT_TIMESTAMP, INTERVAL 1 DAY))) AS tasks_created_yesterday
		');

		return $this->getStatisticsPrepareData($result);
	}

	private function getStatisticsPrepareData($source)
	{
		$data = [
			'total_users' => $source[0]->total_users,
			'total_lists' => $source[0]->total_lists,
			'tasks_opened' => $source[0]->tasks_opened,
			'tasks_closed' => $source[0]->tasks_closed,
			'lists_created_yesterday' => $source[0]->lists_created_yesterday,
			'tasks_created_yesterday' => $source[0]->tasks_created_yesterday
		];

		return $data;
	}

	private function getEmailsHistoryData()
	{
		$data = DB::select('
			SELECT
				r.request_id,
				r.request_date,
				r.request_message,
				(SELECT COUNT(id) FROM admin_mass_emailing_jobs WHERE request_id = r.request_id) AS total_scheduled,
				(
					SELECT COUNT(id) FROM jobs WHERE id IN (
						SELECT id FROM admin_mass_emailing_jobs WHERE request_id = r.request_id
					)
				)
				AS still_not_send

			FROM admin_mass_emailing AS r

			ORDER BY r.request_id DESC
		');

		return $this->getEmailsHistoryPrepareData($data);
	}

	private function getEmailsHistoryPrepareData($source)
	{
		$data = [];

		if ( ! empty($source[0]) && ! is_null($source[0]->request_id)) {
			foreach ($source as $request) {

				$total_sent = $request->total_scheduled - $request->still_not_send;
				$status = $request->total_scheduled == $total_sent ? 'Sent' : 'Sending';

				$data[] = [
					'request_id' => $request->request_id,
					'request_date' => $request->request_date,
					'request_message' => $request->request_message,
					'total_scheduled' => $request->total_scheduled,
					'total_sent' => $total_sent,
					'status' => $status
				];
			}
		}

		return $data;
	}
}
