<?php

namespace App\Traits;

trait OrderItems
{
	private $orderItemSeparator = ',';
	private $orderRegEx = '/^(?!0)\d+(,(?!0)\d+)*$/';

	/**
	 * Remove one item from an order
	 *
	 * @param string  $order The current order
	 * @param integer $item  The item to remove from the order
	 *
	 * @return string|NULL The new order value or null if no items remained
	 */
	private function deleteOrderItem($order, $item)
	{
		$oldOrderItems = explode(',', $order);
		$newOrderItems = [];
		foreach ($oldOrderItems as $k => $item) {
			if ($item != $item) {
				array_push($newOrderItems, $item);
			}
		}

		if (count($newOrderItems) > 0) {
			return implode(',', $newOrderItems);
		}
		else {
			return NULL;
		}
	}
}
