<?php

namespace App\Traits;

trait SimpleToken
{
	/**
	 * The amount of characters by the hash function output
	 *
	 * @var integer
	 */
	public static $tokenLength = 32;

	/**
	 * Hash algorithm name
	 *
	 * @var string
	 */
	public static $tokenAlgo = 'md5';

	/**
	 * Generate a token
	 *
	 * @param intger The user id used as a grain of 'salt'
	 * @param string The user e-mail address used as a grain of 'salt'
	 *
	 * @return string The generated token
	 */
	public static function generateToken($userId, $userEmail)
	{
		return hash(self::$tokenAlgo, bin2hex(
			$userId .
			$userEmail .
			microtime(true) .
			openssl_random_pseudo_bytes(32)
		));
	}
}
