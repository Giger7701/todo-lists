<?php

namespace App\Traits;

use Illuminate\Validation\Validator;

use App\Exceptions\ValidationException;

trait Validations
{
	private function checkForErrors(Validator $validator)
	{
		$errors = $validator->errors();

		foreach ($errors->toArray() as $name => $err) {
			throw new ValidationException($errors->first($name));
		}
	}
}
