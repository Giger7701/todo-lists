<?php

namespace App;

class Pagination
{
	public static function getDBConditions($totalItems, $itemsPerPage, $pageNumber)
	{
		// just one page
		if ($totalItems <= $itemsPerPage) {
			return [1, 0, $itemsPerPage];
		}

		// the current page have no any items
		// reset to first page
		if ($pageNumber * $itemsPerPage - $itemsPerPage >= $totalItems) {
			$pageNumber = 1;
		}

		return [
			$pageNumber,
			$pageNumber * $itemsPerPage - $itemsPerPage,
			$itemsPerPage
		];
	}
}
