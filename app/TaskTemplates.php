<?php

namespace App;

use Illuminate\{
	Support\Facades\DB
};

class TaskTemplates
{
	/**
	 * Copy tasks from one list to another
	 *
	 * @param integer $fromListId The list id to copy tasks from
	 * @param integer $toListId   The list id to paste tasks to
	 *
	 * @return bool True on success, False otherwise
	 */
	public static function copyFromTo($fromListId, $toListId)
	{
		$result = DB::insert('
			INSERT INTO tasks (list_id, name, completed, created_at, updated_at)
			SELECT ?, name, completed, created_at, updated_at FROM task_templates WHERE list_id = ?
		',[$toListId, $fromListId]);

		return $result;
	}

	/**
	 * Get all list templates
	 * Basic information only: "id" and "name"
	 *
	 * @return array
	 */
	public static function getAllSimple()
	{
		return DB::select('
			SELECT
				id,
				name
			FROM list_templates
		');
	}
}
