<?php

namespace App\Exceptions;

use Exception;

class InvalidOwnerException extends Exception {}
