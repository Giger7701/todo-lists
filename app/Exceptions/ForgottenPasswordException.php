<?php

namespace App\Exceptions;

use Exception;

class ForgottenPasswordException extends Exception {}
