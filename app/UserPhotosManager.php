<?php

namespace App;

use File;

use Illuminate\{
	Support\Facades\Log
};

use App\{
	Exceptions\UserProfileException,
	ImageResizer
};

class UserPhotosManager
{
	private $maxFileSizeAllowed = 512000; // in bytes
	private $fileMimeTypesAllowed = [
		'image/jpeg',
		'image/png'
	];

	/**
	 * Setup a new profile photo
	 *
	 * Which includes validations, resize and/or crop of the image files
	 * and moving the new files to user's image directory
	 *
	 * @param array $photos Contains "photo_big" and "photo_small" file names as first and second value
	 *
	 * @return void
	 */
	public function setupNew($fileLocation, $mimeType, $fileSize, $fileExtension)
	{
		$this->isFileMimeTypeAllowed($mimeType);
		$this->isFileSizeTooLarge($fileSize);

		list($big, $small) = $this->compileFileNames($fileExtension);
		$directoryToStoreFiles = config('filesystems.user_photos_directory');

		// Big
		$imageResizer = new ImageResizer($fileLocation, $fileExtension);
		$imageResizer->resize_image(200, 200, 'crop');
		$imageResizer->save_image($directoryToStoreFiles . $big, 100);

		// Small
		$imageResizer = new ImageResizer($fileLocation, $fileExtension);
		$imageResizer->resize_image(80, 80, 'crop');
		$imageResizer->save_image($directoryToStoreFiles . $small, 100);

		return [$big, $small];
	}

	/**
	 * Delete profile photo files from the file system
	 *
	 * @param array $photos The keys "photo_big" and "photo_small" contains the corresponding file names
	 *
	 * @return void
	 */
	public function deleteProfilePhotos(array $photos)
	{
		$userPhotosDirectory = config('filesystems.user_photos_directory');

		$filesToBeDelete = [
			$userPhotosDirectory . $photos['photo_big'],
			$userPhotosDirectory . $photos['photo_small']
		];

		File::delete($filesToBeDelete);
	}

	/**
	 * Compile the image file names
	 *
	 * @return array First name is the big profile photo, second name is the small profile photo
	 */
	public function compileFileNames($fileExtension)
	{
		$filePrefix = hash('md5', bin2hex(openssl_random_pseudo_bytes(32)));

		return [
			$filePrefix . '-big.' . $fileExtension,
			$filePrefix . '-small.' . $fileExtension
		];
	}

	/**
	 * Check the file size
	 *
	 * @throws UserProfileException In case the file size is too large
	 *
	 * @return void
	 */
	private function isFileSizeTooLarge($fileSize)
	{
		if ($fileSize > $this->maxFileSizeAllowed) {
			throw new UserProfileException('file_too_large');
		}
	}

	/**
	 * Is the MimeType allowed
	 *
	 * @throws UserProfileException In case the MimeType is not allowed
	 *
	 * @return void
	 */
	private function isFileMimeTypeAllowed($fileMimeType)
	{
		if ( ! in_array($fileMimeType, $this->fileMimeTypesAllowed) ) {
			throw new UserProfileException('invalid_file_type');
		}
	}
}
