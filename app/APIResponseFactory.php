<?php

namespace App;

use Illuminate\{
	Support\Facades\Log
};

use App\{
	APIResponse
};

class APIResponseFactory
{
	public static function makeForbiddenAccount($ex)
	{
		Log::info($ex->getMessage());

		$apiResponse = new APIResponse(APIResponse::STATUS_USER_ACCOUNT);
		$apiResponse->setMessage($ex->getMessage());

		return response()->json($apiResponse->getResponse(), APIResponse::CODE_OK);
	}

	public static function makeForbiddenToken($ex)
	{
		Log::info($ex->getMessage());

		$apiResponse = new APIResponse(APIResponse::STATUS_TOKEN_ERROR);
		$apiResponse->setMessage($ex->getMessage());

		return response()->json($apiResponse->getResponse(), APIResponse::CODE_OK);
	}

	public static function makeUserProfileNotFound($ex)
	{
		return self::notFound($ex);
	}

	public static function makeUserNotFound($ex)
	{
		Log::info($ex->getMessage());

		$apiResponse = new APIResponse(APIResponse::STATUS_NOT_FOUND);
		$apiResponse->setMessage('User not found!');

		return response()->json($apiResponse->getResponse(), APIResponse::CODE_OK);
	}

	public static function makeInvalidOwner($ex)
	{
		return self::notFound($ex);
	}

	public static function makeValidationError($ex)
	{
		Log::info($ex->getMessage());

		$apiResponse = new APIResponse(APIResponse::STATUS_INVALID_REQUEST);
		$apiResponse->setMessage($ex->getMessage());

		return response()->json($apiResponse->getResponse(), APIResponse::CODE_OK);
	}

	private static function notFound($ex)
	{
		Log::info($ex->getMessage());

		$apiResponse = new APIResponse(APIResponse::STATUS_NOT_FOUND);
		$apiResponse->setMessage($ex->getMessage());

		return response()->json($apiResponse->getResponse(), APIResponse::CODE_OK);
	}
}
