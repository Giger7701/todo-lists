<?php

namespace App;

use Illuminate\{
	Database\Eloquent\Model,
	Support\Facades\DB,
	Support\Facades\Log
};

class User extends Model
{
	const PASSWORD_MIN_LENGTH = 5;

	/**
	 * Toggle the user flag "active"
	 *
	 * @return void
	 */
	public function toggleActive()
	{
		$this->active = ($this->active == 1) ? 0 : 1;
		$this->save();

		return $this->active;
	}

	/**
	 * Get the user's profile photos
	 *
	 * @return array|bool False on no photos found or array with both "big" and "small" photo files
	 */
	public function getPhotos()
	{
		$photos = $this->hasMany('App\UserPhotos')->getResults();

		if (count($photos) == 0) {
			return false;
		}

		return [
			'big' => $photos[0]['photo_big'],
			'small' => $photos[0]['photo_small']
		];
	}

	public static function getLimitedWithListsCounted($additionalWhere, $orderBy, $startFrom, $maxItems)
	{
		$sql = "
			SELECT
				u.id,
				u.name,
				u.email,
				u.active,
				DATE_FORMAT(DATE(u.created_at), '%d/%m/%Y') AS created_at,
				DATE_FORMAT(DATE(u.updated_at), '%d/%m/%Y') AS updated_at,
				(SELECT COUNT(id) FROM lists WHERE user_id = u.id) AS total_lists
			FROM users AS u
			WHERE u.type = 'user' {$additionalWhere}
			ORDER BY {$orderBy}
			LIMIT ?, ?
		";

		return DB::select($sql, [$startFrom, $maxItems]);
	}

	public static function getTotalUsersFiltered($additionalWhere)
	{
		$sql = "
			SELECT
				COUNT(u.id) AS total_users
			FROM users AS u
			WHERE u.type = 'user' {$additionalWhere}
		";

		$result = DB::select($sql);

		return $result[0]->total_users;
	}
}
