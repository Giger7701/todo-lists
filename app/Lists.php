<?php

namespace App;

use Illuminate\{
	Database\Eloquent\Model,
	Support\Facades\DB,
	Support\Facades\Log
};

use App\{
	Exceptions\InvalidOwnerException
};

class Lists extends Model
{
	// protected $table = 'lists';
	// public $timestamps = true;

	/**
	 * Validates the owner of the list
	 *
	 * @param $listId integer The list id
	 *
	 * @return bool
	 */
	public static function validateOwner($listId, $userId)
	{
		$result = DB::select('SELECT TRUE FROM lists WHERE user_id = ? AND id = ?', [$userId, $listId]);

		if (empty($result)) {
			throw new InvalidOwnerException("List doesn't exist");
		}
	}

	/**
	 * Gets a user's lists from the database
	 *
	 * @param $userId integer The user id
	 *
	 * @return array
	 */
	public static function getUserLists($userId)
	{
		/*
		return self::where('user_id', $userId)->get(['id', 'name', 'hide_completed_items']);
		*/

		$sql = '
			SELECT
				id,
				name,
				hide_completed_items
			FROM lists
			WHERE user_id = ?
			ORDER BY id DESC
		';

		return DB::select($sql, [$userId]);
	}

	/**
	 * Gets a user's lists ordered
	 *
	 * @param $userId    integer The user id
	 * @param $orderList string  The ids to order by (separated by comma)
	 *
	 * @see `order` column in table "lists_order"
	 *
	 * @return array
	 */
	public static function getUserListsOrdered($userId, $orderList)
	{
		$sql = "
			SELECT
				l.id,
				l.name,
				l.hide_completed_items,
				t.`order` AS `tasks_order`
			FROM lists AS l
			JOIN tasks_order AS t ON (t.list_id = l.id)
			WHERE l.user_id = ?
			ORDER BY field (l.id, ${orderList})
		";

		return DB::select($sql, [$userId]);
	}

	public static function getTotalListsFiltered($additionalWhere)
	{
		$sql = "
			SELECT
				COUNT(l.id) AS total_lists
			FROM lists AS l
			JOIN users AS u ON (u.id = l.user_id)
			WHERE u.type = 'user' {$additionalWhere}
		";

		$result = DB::select($sql);

		return $result[0]->total_lists;
	}

	public static function getLimitedWithTasksCounted($additionalWhere, $orderBy, $startFrom, $maxItems)
	{
		$sql = "
			SELECT
				l.id,
				l.name,
				DATE_FORMAT(DATE(l.created_at), '%d/%m/%Y') AS created_at,
				u.name AS user_name,
				(SELECT COUNT(id) FROM tasks WHERE list_id = l.id) AS total_tasks
			FROM lists AS l
			JOIN users AS u ON (u.id = l.user_id)
			WHERE u.type = 'user' {$additionalWhere}
			ORDER BY {$orderBy}
			LIMIT ?, ?
		";

		return DB::select($sql, [$startFrom, $maxItems]);
	}
}
