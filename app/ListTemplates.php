<?php

namespace App;

use Illuminate\{
	Database\Eloquent\Model,
	Support\Facades\DB,
	Support\Facades\Log
};

use App\{
	Exceptions\InvalidOwnerException
};

class ListTemplates extends Model
{
	public static function getAllBasics()
	{
		return self::get(['id', 'name']);

		/*
		return DB::select('
			SELECT
				id,
				name
			FROM list_templates
		');
		*/
	}
}
