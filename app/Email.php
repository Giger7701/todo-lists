<?php

namespace App;

use Carbon;

use Illuminate\{
	Support\Facades\Log,
	Support\Facades\Mail
};

use App\{
	Mail\MassEmailing
};

class Email
{
	/**
	 * Send an e-mail to all users
	 *
	 * @param string $token
	 *
	 * @return void
	 */
	public function sendToAllUsers($from, $subject, $body)
	{
		// get all users (of type 'user')
		$users = User::where([
			'type' => 'user',
			'verified' => 1,
			'active' => 1
		])->get();

		// no users found, skip the e-mail sending
		if (empty($users)) {
			return;
		}

		// add a new Mass E-Mailing record
		$adminMassEmailing = new AdminMassEmailing();
		$adminMassEmailing->request_message = $body;
		$adminMassEmailing->save();

		// initial pause before the first sending
		$time = 10;

		// pause between each sending
		$interval = 3;

		// create a DateTime object
		$when = Carbon\Carbon::now()->addSeconds($time);

		// queue the e-mails
		foreach ($users as $user) {

			// add a mail to the queue
			$job_id = Mail::to($user)->later($when, new MassEmailing($from, $subject, $body));

			// store the mail queue job id
			$adminMassEmailingJobs = new AdminMassEmailingJobs();
			$adminMassEmailingJobs->request_id = $adminMassEmailing->id;
			$adminMassEmailingJobs->job_id = $job_id;
			$adminMassEmailingJobs->save();

			// setup next DateTime object
			$time += $interval;
			$when = Carbon\Carbon::now()->addSeconds($time);
		}
	}

	/**
	 * Send a User Reset Password e-mail
	 *
	 * @param string $token
	 *
	 * @return void
	 */
	public function sendUserResetPassword($token, $email)
	{
		Mail::send('user/emails/user_reset_password', [
			'title' => 'Reset Password',
			'url' => config('app.url') . '/#/reset-password?token=' . $token
		], function ($message) use ($email) {
			$message->subject('Reset your password');
			$message->from(config('mail.from.address'), config('mail.from.name'));
			$message->to($email);
		});
	}

	/**
	 * Send a User Registration verification e-mail
	 *
	 * @param string $token
	 *
	 * @return void
	 */
	public function sendUserRegistration($token, $email)
	{
		Mail::send('user/emails/user_registration', [
			'title' => 'Verify Account',
			'url' => config('app.url') . '/#/login?token=' . $token
		], function ($message) use ($email) {
			$message->subject('Registration confirmation');
			$message->from(config('mail.from.address'), config('mail.from.name'));
			$message->to($email);
		});
	}
}
