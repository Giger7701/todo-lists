<?php

use Illuminate\Database\Seeder;

class TaskTemplatesSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$date = date('Y-m-d H:i:s');

		DB::table('task_templates')->insert([
			['list_id' => 1, 'name' => 'Clean up room', 'completed' => 0, 'created_at' => $date, 'updated_at' => $date],
			['list_id' => 1, 'name' => 'Walk out the dog', 'completed' => 0, 'created_at' => $date, 'updated_at' => $date],
			['list_id' => 1, 'name' => 'Get the dinner ready', 'completed' => 0, 'created_at' => $date, 'updated_at' => $date]
		]);
	}
}
