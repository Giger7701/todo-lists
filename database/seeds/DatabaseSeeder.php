<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$this->call(UsersTableSeeder::class);
		$this->call(UserPhotosTableSeeder::class);
		$this->call(ListsTableSeeder::class);
		$this->call(ListsOrderTableSeeder::class);
		$this->call(TasksTableSeeder::class);
		$this->call(TaksOrderTableSeeder::class);
		$this->call(ListTemplatesSeeder::class);
		$this->call(TaskTemplatesSeeder::class);
	}
}
