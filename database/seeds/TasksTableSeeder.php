<?php

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$date = date('Y-m-d H:i:s');

		DB::table('tasks')->insert([
			['list_id' => 1, 'name' => 'Go shopping', 'completed' => 0, 'created_at' => $date, 'updated_at' => $date],
			['list_id' => 1, 'name' => 'Clean up room', 'completed' => 0, 'created_at' => $date, 'updated_at' => $date],
			['list_id' => 1, 'name' => 'Walk out the dog', 'completed' => 1, 'created_at' => $date, 'updated_at' => $date],
			['list_id' => 1, 'name' => 'Read the session manual', 'completed' => 0, 'created_at' => $date, 'updated_at' => $date],
			['list_id' => 1, 'name' => 'Get the diner ready', 'completed' => 1, 'created_at' => $date, 'updated_at' => $date],
			['list_id' => 1, 'name' => 'Send e-mails', 'completed' => 1, 'created_at' => $date, 'updated_at' => $date],
			['list_id' => 1, 'name' => 'Call the service manager', 'completed' => 0, 'created_at' => $date, 'updated_at' => $date],
			['list_id' => 1, 'name' => 'Complete physical exercises', 'completed' => 0, 'created_at' => $date, 'updated_at' => $date],
			['list_id' => 1, 'name' => 'Send party invitations', 'completed' => 0, 'created_at' => $date, 'updated_at' => $date],
			['list_id' => 1, 'name' => 'Watch the product presentation', 'completed' => 1, 'created_at' => $date, 'updated_at' => $date],
			['list_id' => 1, 'name' => 'Get drunk and eating 1000 chicken nuggets', 'completed' => 1, 'created_at' => $date, 'updated_at' => $date]
		]);

		DB::table('tasks')->insert([
			['list_id' => 2, 'name' => 'Clean up room', 'completed' => 0, 'created_at' => $date, 'updated_at' => $date],
			['list_id' => 2, 'name' => 'Walk out the dog', 'completed' => 0, 'created_at' => $date, 'updated_at' => $date],
			['list_id' => 2, 'name' => 'Get the dinner ready', 'completed' => 0, 'created_at' => $date, 'updated_at' => $date]
		]);

		DB::table('tasks')->insert([
			['list_id' => 3, 'name' => 'Clean up room', 'completed' => 0, 'created_at' => $date, 'updated_at' => $date],
			['list_id' => 3, 'name' => 'Walk out the dog', 'completed' => 0, 'created_at' => $date, 'updated_at' => $date],
			['list_id' => 3, 'name' => 'Get the dinner ready', 'completed' => 0, 'created_at' => $date, 'updated_at' => $date]
		]);

		DB::table('tasks')->insert([
			['list_id' => 4, 'name' => 'Clean up room', 'completed' => 0, 'created_at' => $date, 'updated_at' => $date],
			['list_id' => 4, 'name' => 'Vacuuming', 'completed' => 0, 'created_at' => $date, 'updated_at' => $date],
			['list_id' => 4, 'name' => 'Get the dinner ready', 'completed' => 0, 'created_at' => $date, 'updated_at' => $date],
			['list_id' => 4, 'name' => 'Feeding pets', 'completed' => 0, 'created_at' => $date, 'updated_at' => $date]
		]);

		DB::table('tasks')->insert([
			['list_id' => 5, 'name' => 'Mopping floors', 'completed' => 0, 'created_at' => $date, 'updated_at' => $date],
			['list_id' => 5, 'name' => 'Watering plants', 'completed' => 0, 'created_at' => $date, 'updated_at' => $date],
			['list_id' => 5, 'name' => 'Mowing the lawn', 'completed' => 0, 'created_at' => $date, 'updated_at' => $date],
			['list_id' => 5, 'name' => 'Weeding the garden', 'completed' => 0, 'created_at' => $date, 'updated_at' => $date],
			['list_id' => 5, 'name' => 'Wash the car', 'completed' => 0, 'created_at' => $date, 'updated_at' => $date]
		]);
	}
}
