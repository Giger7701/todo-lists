<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;

class UsersTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$date = new DateTime('now');
		$date->sub(new DateInterval('P2YT12H'));
		// $date->format('Y-m-d H:i:s');

		// $date = date('Y-m-d H:i:s');
		$pass = bcrypt('123123');

		// Admin User
		DB::table('users')->insert([
			'type' => 'admin',
			'name' => 'admin',
			'email' => 'admin@gmail.com',
			'password' => $pass,
			'active' => 1,
			'verified' => 1,
			'created_at' => $date->format('Y-m-d H:i:s'),
			'updated_at' => $date->format('Y-m-d H:i:s')
		]);

		// Regular User(s)
		$date->add(new DateInterval('PT10H'));
		$date->format('Y-m-d H:i:s');
		DB::table('users')->insert([
			'type' => 'user',
			'name' => 'giger',
			'email' => 'giger@gmail.com',
			'password' => $pass,
			'active' => 1,
			'verified' => 1,
			'created_at' => $date->format('Y-m-d H:i:s'),
			'updated_at' => $date->format('Y-m-d H:i:s')
		]);
		$date->add(new DateInterval('PT10H'));
		$date->format('Y-m-d H:i:s');
		DB::table('users')->insert([
			'type' => 'user',
			'name' => 'billy',
			'email' => 'billy@gmail.com',
			'password' => $pass,
			'active' => 1,
			'verified' => 1,
			'created_at' => $date->format('Y-m-d H:i:s'),
			'updated_at' => $date->format('Y-m-d H:i:s')
		]);

		// Generate some random records
		/*
		$user = factory(App\User::class, 98)->create();
		*/
		$data = [];
		for ($i = 4; $i < 101; $i++) {
			$date->add(new DateInterval('P7DT6H'));
			$user = str_random( random_int(3, 20) );
			$mail = str_random( random_int(3, 20) );
			$data[] = [
				'id' => $i,
				'type' => 'user',
				'name' => $user,
				'email' => $mail . '@gmail.com',
				'password' => $pass,
				'active' => 1,
				'verified' => 1,
				'created_at' => $date->format('Y-m-d H:i:s'),
				'updated_at' => $date->format('Y-m-d H:i:s')
			];
		}
		DB::table('users')->insert($data);
	}
}
