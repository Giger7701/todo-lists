<?php

use Illuminate\Database\Seeder;

class ListTemplatesSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$date = date('Y-m-d H:i:s');

		DB::table('list_templates')->insert([
			['name' => 'Daily Chores', 'created_at' => $date, 'updated_at' => $date]
		]);
	}
}
