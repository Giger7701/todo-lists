<?php

use Illuminate\Database\Seeder;

class ListsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$date = date('Y-m-d H:i:s');

		DB::table('lists')->insert([
			['id' => 1, 'user_id' => 2, 'name' => 'List name One', 'created_at' => $date, 'updated_at' => $date],
			['id' => 2, 'user_id' => 2, 'name' => 'List name Two', 'created_at' => $date, 'updated_at' => $date],
			['id' => 3, 'user_id' => 3, 'name' => 'My List 1', 'created_at' => $date, 'updated_at' => $date],
			['id' => 4, 'user_id' => 3, 'name' => 'My List 2', 'created_at' => $date, 'updated_at' => $date],
			['id' => 5, 'user_id' => 3, 'name' => 'Test List', 'created_at' => $date, 'updated_at' => $date]
		]);
	}
}
