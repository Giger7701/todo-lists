<?php

use Illuminate\Database\Seeder;

class UserPhotosTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$date = date('Y-m-d H:i:s');

		DB::table('user_photos')->insert([
			'user_id' => 1,
			'photo_big' => NULL,
			'photo_small' => NULL,
			'created_at' => $date,
			'updated_at' => $date
		]);

		DB::table('user_photos')->insert([
			'user_id' => 2,
			'photo_big' => '73f957f72af8ad52d8e70f5e216e4eae-big.jpeg',
			'photo_small' => '73f957f72af8ad52d8e70f5e216e4eae-small.jpeg',
			'created_at' => $date,
			'updated_at' => $date
		]);

		// Generate some random records
		$data = [];
		for ($i = 3; $i < 101; $i++) {
			$data[] = [
				'user_id' => $i,
				'photo_big' => NULL,
				'photo_small' => NULL,
				'created_at' => $date,
				'updated_at' => $date
			];
		}
		DB::table('user_photos')->insert($data);
	}
}
