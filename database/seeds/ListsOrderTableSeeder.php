<?php

use Illuminate\Database\Seeder;

class ListsOrderTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('lists_order')->insert([
			'user_id' => 1,
			'order' => NULL
		]);

		DB::table('lists_order')->insert([
			'user_id' => 2,
			'order' => '2,1'
		]);

		// Generate some random records
		$data = [];
		for ($i = 3; $i < 101; $i++) {
			$data[] = [
				'user_id' => $i,
				'order' => NULL
			];
		}
		DB::table('lists_order')->insert($data);
	}
}
