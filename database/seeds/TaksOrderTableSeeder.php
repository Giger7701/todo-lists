<?php

use Illuminate\Database\Seeder;

class TaksOrderTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('tasks_order')->insert([
			['list_id' => 1, 'order' => '1,2,3,4,5,6,7,8,9,10,11'],
			['list_id' => 2, 'order' => NULL]
		]);
	}
}
