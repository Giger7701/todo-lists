<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminMassEmailingTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('admin_mass_emailing', function (Blueprint $table) {
			$table->increments('request_id')->unsigned();
			$table->dateTime('request_date')->default(DB::raw('NOW()'));
			$table->string('request_message', 1024);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('admin_mass_emailing');
	}
}
