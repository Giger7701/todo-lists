<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function (Blueprint $table) {
			$table->engine = 'InnoDB';

			$table->increments('id')->unsigned();
			$table->enum('type', ['user', 'admin'])->default('user');
			$table->string('name', 30)->unique();
			$table->string('email', 120)->unique();
			$table->char('password', 60);
			$table->boolean('active')->default(1);
			$table->boolean('verified')->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
	}
}
