<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminMassEmailingJobsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('admin_mass_emailing_jobs', function (Blueprint $table) {
			$table->bigIncrements('id')->unsigned();
			$table->unsignedInteger('request_id')->comment('admin_mass_emailing.request_id');
			$table->unsignedInteger('job_id')->comment('jobs.id');

			$table->foreign('request_id')->references('request_id')->on('admin_mass_emailing')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('admin_mass_emailing_jobs');
	}
}
