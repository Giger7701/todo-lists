<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskTemplates extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('task_templates', function (Blueprint $table) {
			$table->engine = 'InnoDB';

			$table->increments('id')->unsigned();
			$table->integer('list_id')->unsigned();
			$table->string('name', 128);
			$table->boolean('completed')->default(0);
			$table->timestamps();

			$table->foreign('list_id')->references('id')->on('list_templates')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('task_templates');
	}
}
