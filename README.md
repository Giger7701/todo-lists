# To Do Lists

This is a test **To Do** web application. Build on top of **Laravel v5.5** and **VueJS v2.5**.

**NOTE:** Basic knowledge about Laravel configuration settings is required!

## Requirements
- **_PHP v7.0_** or newer
- **_MySQL v5.7_** or newer

## Features
### User section
- Registration with E-Mail verification, Forgotten/Reset Password and Login

##### Account section
- Change Password
- Change and Remove profile image

##### To Do section
- Create, Edit and Delete Lists with option to inherit Tasks from other Lists
- Mark Tasks as completed
- Hide completed tasks
- Re-Arrange Lists and Tasks

### Admin section
- Login

##### Dashboard
- Basic usage statistics
- E-Mail All Users
- Mass E-Mails History

##### Users

- Single or multiple Delete, Suspend and Unsuspend users at a  time
- Search by phrase
- Filter by Active and Inactive users
- Basic User's Lists statistics
- Login as this user feature
- Sorting and Pagination

##### Lists
- Single or multiple Delete lists at a time
- Search by phrase
- Filter by User
- Basic User's Tasks statistics
- Sorting and Pagination

## Installation
Application configuration is required for proper use:

0. Database credentials in /config/database.php
0. Driver, Host, E-Mail from Address and Name in /config/mail.php
0. URL and Name in /config/app.php

### Database setup command(s)

0. php artisan migrate --seed

### E-Mails setup
With no direct access to the web server, a cron job is required to check the "**queue:work**" status.
This feature is required by the Admin's **E-Mail All Users** functionality.
For more info see "**App\Console\Kernel->schedule()**"

**Cron job:** \* \* \* \* \* \* /path/to/php /path/to/laravel/artisan schedule:run >> /dev/null 2>&1

### Filesystem Permission(s)

The directory **public/assets/imgs/users** must have "write" permission

### NodeJS modules
In case something goes wrong and the application throw errors.
Remove the directory "node_modules" and run the following commands:

0. npm install --only=dev
0. npm run prod

### Local dev/test server

0. php artisan serve
