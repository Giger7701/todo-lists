Vue.component('status-message', require('./shared/StatusMessage.vue'));
Vue.component('loading-message', require('./shared/LoadingMessage.vue'));
Vue.component('credentials-reset-password', require('./user/CredentialsResetPassword.vue'));
Vue.component('credentials-forgotten-password', require('./user/CredentialsForgottenPassword.vue'));
Vue.component('credentials-login', require('./user/CredentialsLogin.vue'));
Vue.component('credentials-registration', require('./user/CredentialsRegistration.vue'));
Vue.component('user-account', require('./user/UserAccount.vue'));
Vue.component('todo-list-editor', require('./user/ToDoListEditor.vue'));
Vue.component('todo-lists-simple', require('./user/ToDoListsSimple.vue'));
Vue.component('todo-list-content', require('./user/ToDoListContent.vue'));
Vue.component('todo-list', require('./user/ToDoList.vue'));
Vue.component('header-section', require('./user/HeaderSection.vue'));


// ==================================================================
// Application Routes and Router
// ==================================================================

const beforeRouteEnter = function (to, from, next) {
	// console.dir(from);
	// console.dir(to);
	next( vm => {
		// console.dir(vm);
		vm.$root.guardHandler();
	});
};

const CredentialsResetPassword = {
	template: `
		<credentials-reset-password ref="resetPassword" v-if="$root.resetPassword.isActive"></credentials-reset-password>
	`,
	name: 'CredentialsResetPasswordComponent',
	beforeRouteEnter
};

const CredentialsForgottenPassword = {
	template: `
		<credentials-forgotten-password ref="forgottenPassword" v-if="$root.forgottenPassword.isActive"></credentials-forgotten-password>
	`,
	name: 'CredentialsForgottenPasswordComponent',
	beforeRouteEnter
};

const CredentialsLogin = {
	template: `
		<credentials-login ref="login" v-if="$root.login.isActive"></credentials-login>
	`,
	name: 'CredentialsLoginComponent',
	beforeRouteEnter
};

const CredentialsRegistration = {
	template: `
		<credentials-registration ref="registration" v-if="$root.registration.isActive"></credentials-registration>
	`,
	name: 'CredentialsRegistrationComponent',
	beforeRouteEnter
};

const ToDoListsContent = {
	template: `
		<todo-list ref="todo" v-if="$root.todo.isActive"></todo-list>
	`,
	name: 'TodoListComponent',
	beforeRouteEnter
};

const AccountContent = {
	template: `
		<user-account ref="account" v-if="$root.account.isActive"></user-account>
	`,
	name: 'AccountComponent',
	beforeRouteEnter
};

const DefaultHeaderContent = {
	template: `
		<header-section ref="header" v-if="$root.header.isActive"></header-section>
	`,
	name: 'HeaderComponent'
	// the default component in the route using this component will pass through this "guard" no need to do it twice
	// beforeRouteEnter
};

const routes = [
	{ path: '/login', name: 'login', meta: { requiresAuth: false }, components: {
		default: CredentialsLogin
	}},
	{ path: '/reset-password', name: 'resetpassword', meta: { requiresAuth: false }, components: {
		default: CredentialsResetPassword
	}},
	{ path: '/forgotten-password', name: 'forgottenpassword', meta: { requiresAuth: false }, components: {
		default: CredentialsForgottenPassword
	}},
	{ path: '/registration', name: 'registration', meta: { requiresAuth: false }, components: {
		default: CredentialsRegistration
	}},
	{ path: '/todolists', alias: '/', name: 'todolists', meta: { requiresAuth: true }, components: {
		default: ToDoListsContent,
		headerView: DefaultHeaderContent
	}},
	{ path: '/account', name: 'account', meta: { requiresAuth: true }, components: {
		default: AccountContent,
		headerView: DefaultHeaderContent
	}},
	// { path: '/', redirect: { name: 'todolists' }}, // this or alias
	{ path: '*', redirect: '/' }
];

const router = new VueRouter({
	routes: routes,
	mode: 'hash',
	base: '/'
});


// ==================================================================
// Application Instance
// ==================================================================

const app = new Vue({
	router: router,
	el: '#app',
	created () {
		this.init();
	},
	data: {

		// ----------------------------------------
		// Credentials-Reset-Password
		resetPassword: {
			isActive: false,
			data: {}
		},

		// ----------------------------------------
		// Credentials-Forgotten-Password
		forgottenPassword: {
			isActive: false,
			data: {}
		},

		// ----------------------------------------
		// Credentials-Login
		login: {
			isActive: false,
			data: {}
		},

		// ----------------------------------------
		// Credentials-Registration
		registration: {
			isActive: false,
			data: {}
		},

		// ----------------------------------------
		// User-Account
		account: {
			isActive: false,
			data: {
				userProfileImageBig: null
			}
		},

		// ----------------------------------------
		// ToDo-Lists
		todo: {
			isEditListActive: false,
			isEditTaskActive: false,
			isActive: false,
			data: {
				activeListId: null,
				templates: null,
				lists: []
			}
		},

		// ----------------------------------------
		// Header-Block
		header: {
			isActive: false,
			data: {
				username: '',
				userProfileImageSmall: 'default-small.png'
			}
		},

		// ----------------------------------------
		// Generic
		sessionId: null,
		isUserLogged: null,
		loading: true,
		userProfileImagePath: '/assets/imgs/users/',
		userProfileImageBigDefault: 'default-big.png',
		userProfileImageSmallDefault: 'default-small.png',
		sectionsLoaded: {
			// these sections require data loaded from the server
			// each becomes true when the data loads successfully
			// see setLoaded() for more info
			header: false,
			todo: false,
			todo_templates: false
		},
		messageCollection: {
			user_not_found: 'User not found',
			account_inactive: 'This account is currently disabled!',
			missing_auth_token: 'Missing authentication token',
			pass_changed: 'Password successfully changed',
			invalid_verification_token: 'Invalid verification token',
			account_verified: 'Account successfully verified!',
			account_not_verified: 'This account is not verified yet! <br> Please, check you e-mail.',
			invalid_expired_reset_pass_token: 'Invalid or expired reset password token',
			pass_change_success: 'Password changed successfully! <br> You will be redirected to the login page in few seconds.',
			missing_name: 'List name is required!',
			missing_list: 'The list to copy items from doesn\'t exist!',
			missing_current_password: 'Current Password is required!',
			missing_new_password: 'New Password is required!',
			missing_repeat_password: 'Repeat Password is required!',
			invalid_current_password: 'Invalid current password!',
			both_passwords_donot_match: 'Both passwords do not match!',
			invalid_file_type: 'Invalid file type',
			file_too_large: 'File too large',
			file_uploaded_successfully: 'File uploaded successfully',
			file_not_selected: 'File not selected',
			user_exist: 'Username already exists',
			both_pass_req: 'Both passwords are required',
			reg_accepted: 'Please, check your e-mail to complete the registration.',
			invalid_credentials: 'Invalid Username or Password',
			username_req: 'Username is required',
			password_req: 'Password is required',
			email_req: 'E-Mail address is required',
			invalid_email: 'Invalid e-mail address',
			email_sent: 'E-Mail sent'
		}
	},
	methods: {

		// ----------------------------------------
		// Creadentials-Reset-Password

		resetPasswordActivate () {
			this.resetPassword.isActive = true;
		},


		// ----------------------------------------
		// Creadentials-Forgotten-Password

		forgottenPasswordActivate () {
			this.forgottenPassword.isActive = true;
		},


		// ----------------------------------------
		// Creadentials-Login

		loginActivate () {
			this.login.isActive = true;
		},


		// ----------------------------------------
		// Creadentials-Registration

		registrationActivate() {
			this.registration.isActive = true;
		},


		// ----------------------------------------
		// ToDo-Lists

		todoSplitTasks (tasksSource) {

			var tasks = {
				completed: [],
				incompleted: []
			};

			for (var t in tasksSource) {
				if (tasksSource[t].completed == 1) {
					tasks.completed.push(tasksSource[t]);
				}
				else {
					tasks.incompleted.push(tasksSource[t]);
				}
			}

			return tasks;
		},

		todoUpdateTasksOrder (listId, completed, incompleted) {

			var tasksOrder = [];
			for (var i in incompleted) {
				tasksOrder.push( incompleted[i].id );
			}
			for (var i in completed) {
				tasksOrder.push( completed[i].id );
			}
			tasksOrder = tasksOrder.join(',');

			if (this.validateSession()) {return;}
			axios.put(
				this.$root.getRequestUri('/tasks/order'),
				this.$root.setupRequestParams({
					listId: listId,
					tasksOrder: tasksOrder
				}),
				this.$root.setupRequestConfig()
			).then(this.$root.responseMiddleware).then( response => {
				// console.dir(response);
			}).catch(this.responseErrorHandler);
		},

		todoUpdateListsOrder () {

			var listsOrder = [];
			for (var i in this.todo.data.lists) {
				listsOrder.push( this.todo.data.lists[i].id );
			}
			listsOrder = listsOrder.join(',');

			if (this.validateSession()) {return;}
			axios.put(
				this.$root.getRequestUri('/lists/order'),
				this.$root.setupRequestParams({
					listsOrder: listsOrder
				}),
				this.$root.setupRequestConfig()
			).then(this.$root.responseMiddleware).then( response => {
				// console.dir(response);
			}).catch(this.responseErrorHandler);
		},

		todoSetActiveList () {
			if (this.todo.data.lists.length > 0) {
				this.todo.data.activeListId = this.todo.data.lists[0].id;
			}
			else {
				this.todo.data.activeListId = null;
			}
		},

		todoRemoveList (listId) {
			for (var i in this.todo.data.lists) {
				if (this.todo.data.lists[i].id == listId) {
					this.todo.data.lists.splice(i, 1);
					break;
				}
			}
		},

		todoFindList (listId) {
			for (var i in this.todo.data.lists) {
				if (this.todo.data.lists[i].id == listId) {
					return this.todo.data.lists[i];
				}
			}
		},

		todoFindTask (tasks, taskId) {
			for (var i in tasks) {
				if (tasks[i].id == taskId) {
					return tasks[i];
				}
			}
		},

		todoGetCurrentList () {
			for (var i in this.todo.data.lists) {
				if (this.todo.data.lists[i].id == this.todo.data.activeListId) {
					return this.todo.data.lists[i];
				}
			}
		},

		todoActivate () {
			this.todo.isActive = true;
			this.account.isActive = true;
		},


		// ----------------------------------------
		// Header-Section

		headerActivate() {
			this.header.isActive = true;
		},


		// ----------------------------------------
		// Content Loaders

		loadUserContent () {
			this.loadContentHeader();
			this.loadContentToDo();
			this.loadContentToDoTemplates();
		},

		loadContentHeader () {
			if (this.validateSession()) {return;}
			axios.get(
				this.getRequestUri('/'),
				this.$root.setupRequestConfig()
			).then(this.$root.responseMiddleware).then( response => {
				// console.dir(response);
				this.header.data.username = response.data.data.username;
				if (typeof response.data.data.userProfileImage == 'object') {
					this.setUserProfileImages(response.data.data.userProfileImage);
				}
				this.setLoaded('header');
			}).catch(this.responseErrorHandler);
		},

		loadContentToDo () {
			if (this.validateSession()) {return;}
			axios.get(
				this.$root.getRequestUri('/lists'),
				this.$root.setupRequestConfig()
			).then(this.$root.responseMiddleware).then( response => {
				// console.dir(response);
				if (typeof response.data.data.lists === 'object' && response.data.data.lists.length > 0) {
					this.todo.data.lists = response.data.data.lists;
					this.todo.data.activeListId = this.todo.data.lists[0].id;
					for (var i in this.todo.data.lists) {
						this.todo.data.lists[i].tasks = this.$root.todoSplitTasks(this.todo.data.lists[i].tasks);
					}
				}
				this.setLoaded('todo');
			}).catch(this.responseErrorHandler);
		},

		loadContentToDoTemplates () {
			if (this.validateSession()) {return;}
			axios.get(
				this.getRequestUri('/lists/templates'),
				this.$root.setupRequestConfig()
			).then(this.$root.responseMiddleware).then( response => {
				// console.dir(response);
				this.todo.data.templates = response.data.data.lists;
				this.setLoaded('todo_templates');
			}).catch(this.responseErrorHandler);
		},


		// ----------------------------------------
		// Generic

		gotoAccount () {
			this.$router.push({ name: 'account' });
		},

		gotoToDoLists () {
			this.$router.push({ name: 'todolists' });
		},

		gotoForgottenPassword () {
			this.$router.push({ name: 'forgottenpassword' });
		},

		gotoRegistration () {
			this.$router.push({ name: 'registration' });
		},

		gotoLogin () {
			this.$router.push({ name: 'login' });
		},

		setLoaded (name) {
			this.sectionsLoaded[name] = true;

			for (var i in this.sectionsLoaded) {
				if (this.sectionsLoaded[i] == false) {
					return;
				}
			}

			// hide loading
			this.loading = false;

			// activate all the sections that requires content loaded from the server
			this.activateUserSection();
		},

		resetUserProfileImages () {
			this.header.data.userProfileImageSmall = null;
			this.account.data.userProfileImageBig = null;
		},

		setUserProfileImages (images) {
			this.header.data.userProfileImageSmall = images.small;
			this.account.data.userProfileImageBig = images.big;
		},

		apiCallErrorHandler (response) {
			switch (response.data.status) {
				case 'NOT_FOUND':
				case 'INVALID_REQUEST':
					if (typeof response.data.message !== 'undefined') {
						alert(response.data.message);
					}
					document.location.reload(true); // UX?
					break;
				default:
					alert('Request status value not defined!');
			}
		},

		init () {
			// the browser meet requirements?
			// html5 web Storage, History, and base64 de/encoding
			// currently only "Storage" is used
			if (
				typeof(Storage) !== 'function'
				||
				typeof(History) !== 'function'
				||
				typeof(window.atob) !== 'function'
				||
				typeof(window.btoa) !== 'function'
			) {
				this.hideLoading();
				alert('To work properly this application requires HTML5 Web Storage feature! Please, upgrade your browser to the latest version and try again.');
				return;
			}

			// start the application
			this.start();
		},

		start () {
			// we might have a valid session id
			if (this.storageIsSessionIdSet()) {

				var sessionId = this.storageGetSessionId();

				// let's validate
				axios.post(
					this.getRequestUri('/init'),
					this.setupRequestParams(),
					{ headers: {'Authorization': 'Bearer ' + sessionId} }
				).then( response => {
					// console.dir(response);

					if (response.data.status == 'OK') {
						this.setSessionId(sessionId);
						this.setUserLogin();
						this.loadUserContent();
					}
					else {
						this.hideLoading();
						this.destroySession();
						this.activateCredentialsSection();
						this.redirectToLogin();
					}

					// redirect to default section depending on user session
					this.guardHandler();

				}).catch(this.responseErrorHandler);
			}
			// sure thing we don't have, redirect to the login section
			else {
				this.setUserLogout();
				this.hideLoading();
				this.activateCredentialsSection();
				this.guardHandler();
			}
		},

		activateUserSection () {
			this.headerActivate();
			this.todoActivate();
		},

		redirectToHome () {
			this.gotoToDoLists();
		},

		activateCredentialsSection () {
			this.loginActivate();
			this.registrationActivate();
			this.forgottenPasswordActivate();
			this.resetPasswordActivate();
		},

		hideLoading () {
			this.loading = false;
		},

		redirectToLogin () {
			// hide loading
			this.hideLoading();

			// activate credentials' sections
			this.activateCredentialsSection();

			// and open login section
			this.gotoLogin();
		},

		setUserLogin () {
			this.isUserLogged = true;
		},

		setUserLogout () {
			this.isUserLogged = false;
		},

		destroySession () {
			this.removeSessionId();
			if (this.storageIsSessionIdSet()) {
				this.storageRemoveSessionId();
			}
		},

		endSession () {
			this.setUserLogout();
			this.destroySession();
			this.redirectToLogin();
		},

		validateSession () {
			// this will check the session stored in the application and this in the localStorage
			// if both aren't equal then logout and redirect to login section
			// this might happend when the user is logged in with different accounts in different browser tabs
			// this will most likely happen when an admin is using "login as this user" functionality
			if (this.getSessionId() != this.storageGetSessionId()) {
				this.endSession();
				return true;
			}
		},

		removeSessionId () {
			this.sessionId = null;
		},

		getSessionId () {
			return this.sessionId;
		},

		setSessionId (sessionId) {
			this.sessionId = sessionId;
		},

		storageGetSessionId () {
			return window.localStorage.getItem('sessionId');
		},

		storageSetSessionId (sessionId) {
			window.localStorage.setItem('sessionId', sessionId);
		},

		storageRemoveSessionId () {
			window.localStorage.removeItem('sessionId');
		},

		storageIsSessionIdSet () {
			return typeof window.localStorage.getItem('sessionId') === "string" && window.localStorage.getItem('sessionId').length > 0;
		},

		authLogout() {
			/*
			if (this.validateSession()) {return;}
			axios.post(
				this.getRequestUri('/logout'),
				this.setupRequestParams(),
				this.setupRequestConfig
			).then( response => {
				// console.dir(response);
				this.endSession();
			}).catch(this.responseErrorHandler);
			*/

			this.endSession();
		},

		guardHandler () {
			// content loading finished?
			// 'boolean' yes, 'null' no
			if (typeof this.isUserLogged === 'boolean') {

				// redirect to default section depending on user's login status
				if (this.isUserLogged) {
					// inexact/unknown route or section that requires no login, go to home
					if (this.$route.name == null || this.$route.meta.requiresAuth == false) {
						this.redirectToHome();
					}
				}
				else {
					// inexact/unknown route or section that requires login, go to login
					if (this.$route.name == null || this.$route.meta.requiresAuth == true) {
						this.redirectToLogin();
					}
				}
			}
		},

		getRequestUri (suffix) {
			return '/api/user' + suffix;
		},

		setupRequestConfig (params) {

			if (typeof params !== 'object') {
				params = {
					headers: {'Authorization': 'Bearer ' + this.getSessionId()}
				};
			}
			else if (typeof params.headers !== 'object') {
				params.headers = {'Authorization': 'Bearer ' + this.getSessionId()};
			}
			else {
				params.headers['Authorization'] = 'Bearer ' + this.getSessionId();
			}

			return params;
		},

		setupRequestParams (params) {

			if (typeof params === 'undefined') {
				params = {};
			}

			return params;
		},

		responseMiddleware (response) {
			switch (response.data.status) {
				case 'INVALID_REQUEST':
				case 'NOT_FOUND':
				case 'USER_ACCOUNT':
				case 'TOKEN_ERROR':
					this.endSession();
					return Promise.reject(new Error('Error'));
			}

			return response;
		},

		validateSimpleToken (token) {
			var pattern = /^[a-fA-F0-9]{32}$/;
			return pattern.test(token);
		},

		validateEmail (email) {
			var regexPattern = /^(?:(?:[\w`~!#$%^&*\-=+;:{}'|,?\/]+(?:(?:\.(?:"(?:\\?[\w`~!#$%^&*\-=+;:{}'|,?\/\.()<>\[\] @]|\\"|\\\\)*"|[\w`~!#$%^&*\-=+;:{}'|,?\/]+))*\.[\w`~!#$%^&*\-=+;:{}'|,?\/]+)?)|(?:"(?:\\?[\w`~!#$%^&*\-=+;:{}'|,?\/\.()<>\[\] @]|\\"|\\\\)+"))@(?:[a-zA-Z\d\-]+(?:\.[a-zA-Z\d\-]+)*|\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\])$/;
			return regexPattern.test(email);
		},

		// Response error handler
		responseErrorHandler (error) {
			console.dir(error);
		},
	}
});
