Vue.component('status-message', require('./shared/StatusMessage.vue'));
Vue.component('loading-message', require('./shared/LoadingMessage.vue'));
Vue.component('lists-section', require('./admin/ListsSection.vue'));
Vue.component('users-section', require('./admin/UsersSection.vue'));
Vue.component('dashboard-section', require('./admin/DashboardSection.vue'));
Vue.component('pagination', require('./admin/Pagination.vue'));
Vue.component('menu-section', require('./admin/MenuSection.vue'));
Vue.component('header-section', require('./admin/HeaderSection.vue'));
Vue.component('login-section', require('./admin/LoginSection.vue'));


// ==================================================================
// Application Routes and Router
// ==================================================================

const beforeRouteEnter = function (to, from, next) {
	next( vm => {
		vm.$root.guardHandler();
	});
};

const LoginSection = {
	template: `
		<login-section ref="login" v-if="$root.login.isActive"></login-section>
	`,
	beforeRouteEnter
};

const HeaderSection = {
	template: `
		<header-section ref="header" v-if="$root.header.isActive"></header-section>
	`
};

const MenuSection = {
	template: `
		<menu-section ref="menu" v-if="$root.menu.isActive"></menu-section>
	`
};

const DashboardSection = {
	template: `
		<dashboard-section ref="dashboard" v-if="$root.dashboard.isActive"></dashboard-section>
	`,
	beforeRouteEnter
};

const UsersSection = {
	template: `
		<users-section ref="users" v-if="$root.users.isActive"></users-section>
	`,
	beforeRouteEnter
};

const ListsSection = {
	template: `
		<lists-section ref="lists" v-if="$root.lists.isActive"></lists-section>
	`,
	beforeRouteEnter
};

const routes = [
	{ path: '/login', name: 'login', meta: { requiresAuth: false }, components: {
		default: LoginSection
	}},
	{ path: '/dashboard', alias: '/', name: 'dashboard', meta: { requiresAuth: true }, components: {
		default: DashboardSection,
		headerView: HeaderSection,
		menuView: MenuSection
	}},
	{ path: '/users', name: 'users', meta: { requiresAuth: true }, components: {
		default: UsersSection,
		headerView: HeaderSection,
		menuView: MenuSection
	}, children: [
		{ path: ':name', children: [
			{ path: ':value' }
		]}
	]},
	{ path: '/lists', name: 'lists', meta: { requiresAuth: true }, components: {
		default: ListsSection,
		headerView: HeaderSection,
		menuView: MenuSection
	}, children: [
		{ path: 'user/:id' }
	]},
	// { path: '/', redirect: { name: 'dashboard' }}, // this or alias
	{ path: '*', redirect: '/' }
];

const router = new VueRouter({
	routes: routes,
	mode: 'hash',
	base: '/'
});


// ==================================================================
// Application Instance
// ==================================================================

const app = new Vue({
	router: router,
	el: '#app',
	created () {
		this.init();
	},
	mounted () {},
	methods: {

		loginAsThisUser (userId) {
			if (this.validateSession()) {return;}
			axios.post(
				this.getRequestUri('/users/loginas/' + userId),
				this.$root.setupRequestParams(),
				this.setupRequestConfig()
			).then(this.responseMiddleware).then( response => {
				this.storageSetSessionId(response.data.data.token);
				document.location.href = "../";
			}).catch(this.responseErrorHandler);
		},

		loadUserContent () {
			this.loadContentDashboard();
			this.loadContentUsers('setLoaded');
			this.loadContentLists();
		},

		getListTasks (listId) {

			var list = this.listsFindById(listId);
			if (list === false) {
				return false;
			}

			if (typeof list.tasks !== 'undefined') {
				return list.tasks;
			}

			this.loadContentListTasks(listId);
			return false;
		},

		listsFindById (listId) {
			for (var i in this.lists.state.lists) {
				if (this.lists.state.lists[i].id == listId) {
					return this.lists.state.lists[i];
				}
			}
			return false;
		},

		usersResetStateSearch () {
			this.users.state.search = '';
		},

		usersResetStateFilter () {
			this.users.state.filter = 'all';
		},

		usersResetSort () {
			this.users.state.sort = {
				active: 'id',
				direction: {
					asc: false,
					desc: true
				}
			};
		},

		listsResetStateFilter () {
			this.lists.state.filter = 0;
		},

		listsResetStateSearch () {
			this.lists.state.search = '';
		},

		listsResetSort () {
			this.lists.state.sort = {
				active: 'id',
				direction: {
					asc: false,
					desc: true
				}
			};
		},

		loadContentListTasks (listId) {
			if (this.validateSession()) {return;}
			axios.get(
				this.getRequestUri('/lists/' + listId + '/tasks'),
				this.setupRequestConfig()
			).then(this.responseMiddleware).then( response => {
				// console.dir(response);
				this.listsFindById(listId).tasks = response.data.data;
				this.findChild('lists').displayTasks(response.data.data);
			}).catch(this.responseErrorHandler);
		},

		loadContentLists () {
			this.loadContentListsData('setLoaded');
			this.loadContentListsUsers();
		},

		loadContentListsUsers () {
			if (this.validateSession()) {return;}
			axios.get(
				this.getRequestUri('/lists/users'),
				this.setupRequestConfig()
			).then(this.responseMiddleware).then( response => {
				this.lists.users = response.data.data;
				this.setLoaded('lists_users');
			}).catch(this.responseErrorHandler);
		},

		loadContentListsData (callback) {
			var params = {
				sortColumn: this.lists.state.sort.active,
				sortDirection: (this.lists.state.sort.direction.desc == true) ? 'desc' : 'asc',
				itemsPerPage: this.lists.state.showPerPage,
				pageNumber: this.lists.state.page
			};
			if (this.lists.state.search.length > 0) {
				params.searchQuery = this.lists.state.search;
			}
			if (this.lists.state.filter > 0) {
				params.filterByUserId = this.lists.state.filter;
			}

			if (this.validateSession()) {return;}
			axios.get(
				this.getRequestUri('/lists'),
				this.setupRequestConfig({
					params: params
				})
			).then(this.responseMiddleware).then( response => {
				this.lists.state.lists = response.data.data.lists;
				this.lists.state.total = response.data.data.total_lists;
				this.lists.state.page = parseInt(response.data.data.page);
				if (typeof callback === 'function') {
					callback();
				}
				else if (callback === 'setLoaded') {
					this.setLoaded('lists');
				}
			}).catch(this.responseErrorHandler);
		},

		loadContentUsers (callback) {
			var params = {
				sortColumn: this.users.state.sort.active,
				sortDirection: (this.users.state.sort.direction.desc == true) ? 'desc' : 'asc',
				itemsPerPage: this.users.state.showPerPage,
				pageNumber: this.users.state.page
			};
			if (this.users.state.search.length > 0) {
				params.searchQuery = this.users.state.search;
			}
			if (this.users.state.filter !== 'all') {
				params.filterByState = this.users.state.filter;
			}

			if (this.validateSession()) {return;}
			axios.get(
				this.getRequestUri('/users'),
				this.setupRequestConfig({
					params: params
				})
			).then(this.responseMiddleware).then( response => {
				this.users.state.users = response.data.data.users;
				this.users.state.total = response.data.data.total_users;
				this.users.state.page = parseInt(response.data.data.page);
				if (typeof callback === 'function') {
					callback();
				}
				else if (callback === 'setLoaded') {
					this.setLoaded('users');
				}
			}).catch(this.responseErrorHandler);
		},

		loadContentDashboard () {
			this.loadContentDashboardStatistics();
			this.loadContentDashboardEmailsHistory();
		},

		loadContentDashboardStatistics () {
			if (this.validateSession()) {return;}
			axios.get(
				this.getRequestUri('/dashboard/statistics'),
				this.setupRequestConfig()
			).then(this.responseMiddleware).then( response => {
				this.dashboard.data.statistics = response.data.data;
				this.setLoaded('dashboard_statistics');
			}).catch(this.responseErrorHandler);
		},

		loadContentDashboardEmailsHistory () {
			if (this.validateSession()) {return;}
			axios.get(
				this.getRequestUri('/dashboard/emailshistory'),
				this.setupRequestConfig()
			).then(this.responseMiddleware).then( response => {
				this.dashboard.data.emailshistory = response.data.data;
				this.setLoaded('dashboard_emailshistory');
			}).catch(this.responseErrorHandler);
		},

		findMassEmailSendingInProgress () {
			for (var i in this.dashboard.data.emailshistory) {
				if (this.dashboard.data.emailshistory[i].total_scheduled != this.dashboard.data.emailshistory[i].total_sent) {
					return true;
				}
			}

			return false;
		},

		setupRequestConfig (params) {
			if (typeof params !== 'object') {
				params = {
					headers: {'Authorization': 'Bearer ' + this.storageGetSessionId()}
				};
			}
			else if (typeof params.headers !== 'object') {
				params.headers = {'Authorization': 'Bearer ' + this.storageGetSessionId()};
			}
			else {
				params.headers['Authorization'] = 'Bearer ' + this.storageGetSessionId();
			}

			return params;
		},

		setupRequestParams (params) {
			if (typeof params === 'undefined') {
				params = {};
			}
			return params;
		},

		responseMiddleware (response) {
			switch (response.data.status) {
				case 'INVALID_REQUEST':
				case 'NOT_FOUND':
				case 'USER_ACCOUNT':
				case 'TOKEN_ERROR':
					this.endSession();
					return Promise.reject(new Error('Error'));
			}

			return response;
		},

		responseErrorHandler (error) {
			console.dir(error);
		},

		storageGetSessionId () {
			return window.localStorage.getItem('sessionId');
		},

		storageSetSessionId (sessionId) {
			window.localStorage.setItem('sessionId', sessionId);
		},

		storageRemoveSessionId () {
			window.localStorage.removeItem('sessionId');
		},

		storageIsSessionIdSet () {
			return typeof window.localStorage.getItem('sessionId') === "string" && window.localStorage.getItem('sessionId').length > 0;
		},

		getRequestUri (suffix) {
			return '/api/admin' + suffix;
		},

		listsActivate () {
			this.lists.isActive = true;
		},

		usersActivate () {
			this.users.isActive = true;
		},

		dashboardActivate () {
			this.dashboard.isActive = true;
		},

		menuActivate () {
			this.menu.isActive = true;
		},

		headerActivate () {
			this.header.isActive = true;
		},

		loginActivate () {
			this.login.isActive = true;
		},

		loadingDeactivate () {
			this.loading = false;
		},

		redirectToHome () {
			this.gotoDashboard();
		},

		gotoDashboard () {
			this.$router.push({ name: 'dashboard' });
		},

		gotoUsers () {
			this.$router.push({ name: 'users' });
		},

		gotoLists () {
			this.$router.push({ name: 'lists' });
		},

		gotoLogin () {
			this.$router.push({ name: 'login' });
		},

		activate () {
			this.loadingDeactivate();
			this.headerActivate();
			this.menuActivate();
			this.dashboardActivate();
			this.usersActivate();
			this.listsActivate();
		},

		setLoaded (name) {
			this.sectionsLoaded[name] = true;

			for (var i in this.sectionsLoaded) {
				if (this.sectionsLoaded[i] == false) {
					return;
				}
			}

			// activate all the sections that requires content loaded from the server
			this.activate();
		},

		mainmenuSetActive (name) {
			this.findChild('menu').setActiveByRouteName(name);
		},

		findChild (name) {
			for (var i in this.$children) {
				if (typeof this.$children[i].$refs[name] !== 'undefined') {
					return this.$children[i].$refs[name];
				}
			}
		},

		storageSetSessionId (sessionId) {
			window.localStorage.setItem('sessionId', sessionId);
		},

		validateSession () {
			// this will check the session stored in the application and this in the localStorage
			// if both aren't equal then logout and redirect to login section
			// this might happend when the user is logged in with different accounts in different browser tabs
			// this will most likely happen when an admin is using "login as this user" functionality
			if (this.getSessionId() != this.storageGetSessionId()) {
				this.endSession();
				return true;
			}
		},

		removeSessionId () {
			this.sessionId = null;
		},

		getSessionId () {
			return this.sessionId;
		},

		setSessionId (sessionId) {
			this.sessionId = sessionId;
		},

		setUserLogin () {
			this.isUserLogged = true;
		},

		setUserLogout () {
			this.isUserLogged = false;
		},

		destroySession () {
			if (this.storageIsSessionIdSet()) {
				this.storageRemoveSessionId();
			}
		},

		endSession () {
			this.setUserLogout();
			this.destroySession();
			this.redirectToLogin();
		},

		authLogout() {
			/*
			axios.post(
				this.$root.getRequestUri('/logout'),
				this.$root.setupRequestParams(),
				this.$root.setupRequestConfig()
			).then( response => {
				// console.dir(response);
			}).catch(this.$root.responseErrorHandler);
			*/

			this.endSession();
		},

		redirectToLogin () {
			this.loadingDeactivate();
			this.activateCredentialsSection();
			this.gotoLogin();
		},

		activateCredentialsSection () {
			this.loginActivate();
		},

		init () {
			// the browser meet requirements?
			// html5 web Storage, History, and base64 de/encoding
			// currently only "Storage" is used
			if (
				typeof(Storage) !== 'function'
				||
				typeof(History) !== 'function'
				||
				typeof(window.atob) !== 'function'
				||
				typeof(window.btoa) !== 'function'
			) {
				this.loadingDeactivate();
				alert('To work properly this application requires HTML5 Web Storage feature! Please, upgrade your browser to the latest version and try again.');
				return;
			}

			// start the application
			this.start();
		},

		start () {
			// we might have a valid session id
			if (this.storageIsSessionIdSet()) {

				var sessionId = this.storageGetSessionId();

				// let's validate
				axios.post(
					this.getRequestUri('/init'),
					this.setupRequestParams(),
					{ headers: {'Authorization': 'Bearer ' + sessionId} }
				).then( response => {
					// console.dir(response);

					if (response.data.status == 'OK') {
						this.setSessionId(sessionId);
						this.setUserLogin();
						this.loadUserContent();
					}
					else {
						this.loadingDeactivate();
						this.destroySession();
						this.activateCredentialsSection();
						this.redirectToLogin();
					}

					// redirect to default section depending on user session
					this.guardHandler();

				}).catch(this.$root.responseErrorHandler);
			}
			// sure thing we don't have, redirect to the login section
			else {
				this.setUserLogout();
				this.loadingDeactivate();
				this.activateCredentialsSection();
				this.guardHandler();
			}
		},

		guardHandler () {
			// content loading finished?
			// 'boolean' yes, 'null' no
			if (typeof this.isUserLogged === 'boolean') {

				// redirect to default section depending on user's login status
				if (this.isUserLogged) {
					// inexact/unknown route or section that requires no login, go to home
					if (this.$route.name == null || this.$route.meta.requiresAuth == false) {
						this.redirectToHome();
					}
				}
				else {
					// inexact/unknown route or section that requires login, go to login
					if (this.$route.name == null || this.$route.meta.requiresAuth == true) {
						this.redirectToLogin();
					}
				}
			}
		}
	},
	data: {

		// ----------------------------------------
		// Lists
		lists: {
			isActive: false,
			state: {
				search: '',
				filter: 0,
				sort: {
					active: 'id',
					direction: {
						asc: false,
						desc: true
					}
				},
				lists: [],
				total: 0,
				showPerPage: '10',
				page: 1
			},
		},

		// ----------------------------------------
		// Users
		users: {
			isActive: false,
			state: {
				search: '',
				filter: 'all',
				sort: {
					active: 'id',
					direction: {
						asc: false,
						desc: true
					}
				},
				users: [],
				total: 0,
				showPerPage: '10',
				page: 1
			},
			data: {}
		},

		// ----------------------------------------
		// Dashboard
		dashboard: {
			isActive: false,
			refresh: false,
			data: {
				emailshistory: [], // there is no history if this is empty
				statistics: {
					total_users: 0,
					total_lists: 0,
					tasks_opened: 0,
					tasks_closed: 0,
					lists_created_yesterday: 0,
					tasks_created_yesterday: 0
				}
			}
		},

		// ----------------------------------------
		// Main-Menu

		menu: {
			isActive: false
		},

		// ----------------------------------------
		// Header

		header: {
			isActive: false
		},

		// ----------------------------------------
		// Login

		login: {
			isActive: true
		},

		// ----------------------------------------
		// Generic

		messageCollection: {
			account_not_verified: 'This account is not verified yet! <br> Please, check you e-mail.',
			invalid_credentials: 'Invalid Username or Password',
			username_req: 'Username is required',
			password_req: 'Password is required',
			mass_emailing_queued_succ: 'The E-Mails are queued for sending',
			from_required: '"From" is required!',
			subject_required: '"Subject" is required!',
			body_required: '"Body" is required!'
		},

		sectionsLoaded: {
			// these sections require data from the server
			// each becomes true when the data loads successfully
			// see setLoaded() for more info
			dashboard_statistics: false,
			dashboard_emailshistory: false,
			users: false,
			lists: false,
			lists_users: false
		},

		sessionId: null,
		isUserLogged: null,
		loading: true
	}
});
