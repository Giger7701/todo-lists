<html>
	<head>
		<title>{{ $title }}</title>
		<meta charset="UTF-8">
		<style type="text/css">
			body {
				background: white;
				color: black;
			}
			a, a:active, a:hover {
				color: green;
			}
		</style>
	</head>
	<body>
		<p>{{ $title }}</p>
		<a href="{{ $url }}" target="_blank">Click here</a>
	</body>
</html>
