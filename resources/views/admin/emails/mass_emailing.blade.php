<html>
	<head>
		<title>{{ $subject }}</title>
		<meta charset="UTF-8">
		<style type="text/css">
			body {
				background: white;
				color: black;
			}
		</style>
	</head>
	<body>
		<p>{{ $subject }}</p>
		<p style="color:green;">{{ $body }}</p>
	</body>
</html>
