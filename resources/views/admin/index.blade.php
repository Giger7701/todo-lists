<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Admin : To Do Lists</title>

		<link href="https://fonts.googleapis.com/css?family=Noto+Sans" type="text/css" rel="stylesheet">
		<link href="/assets/css/admin-app.css" type="text/css" rel="stylesheet">
	</head>
	<body>

		<div id="app">
			<div class="container">

				<loading-message v-if="loading"></loading-message>
				<router-view name="headerView"></router-view>

				<main>
					<router-view name="menuView"></router-view>
					<router-view></router-view>
				</main>

			</div>
		</div>

		<script src="/assets/js/vendor/axios.js" type="text/javascript"></script>
		<script src="/assets/js/vendor/vue.js" type="text/javascript"></script>
		<script src="/assets/js/vendor/vue-router.js" type="text/javascript"></script>
		<script src="/assets/js/admin-app.js" type="text/javascript"></script>
	</body>
</html>
