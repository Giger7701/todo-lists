let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
	/* user */
	.js('resources/assets/js/user-app.js', 'public/assets/js')
	.sass('resources/assets/sass/user-app.scss', 'public/assets/css')

	/* admin */
	.js('resources/assets/js/admin-app.js', 'public/assets/js')
	.sass('resources/assets/sass/admin-app.scss', 'public/assets/css')
;
