<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin', function () {

	// active admin user
	return view('admin/index');

	// the request uri must not start with 'api'
})->where('any', '^(?!api|user).*');

Route::get('/{user}', function () {

	// active regular user
	// or no user at all
	return view('user/index');

	// the request uri must not start with 'api'
})->where('user', '^(?!api|admin).*');

/*
http://localhost:8000/

http://localhost:8000/user
http://localhost:8000/user/login
http://localhost:8000/user/mylists
http://localhost:8000/user/myaccount

http://localhost:8000/admin
http://localhost:8000/admin/login
http://localhost:8000/admin/dashboard
http://localhost:8000/admin/users
http://localhost:8000/admin/lists
*/
