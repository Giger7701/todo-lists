<?php

// use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// ------------------------------------------------------------------------
// Admin
Route::prefix('/admin')->group( function () {

	// POST /logout
	// no real need to send a request to the server for this
	// nothing happens on server side
	// Route::post('/logout', 'Admin\User\LogoutController@index');

	// POST /login
	Route::post('/login', 'Admin\User\LoginController@index');

	// Authentication required
	Route::group(['middleware' => ['auth.check', 'auth.role:admin']], function () {

		// POST /init
		Route::post('/init', 'Admin\InitController@index');

		// Lists
		Route::prefix('/lists')->group( function () {

			// GET /{id}/tasks
			// return list's tasks
			Route::get('/{id}/tasks', 'Admin\ListsController@fetchTasks');

			// GET /users
			// return a list with all users
			Route::get('/users', 'Admin\ListsController@fetchUsers');

			// DELETE /
			// delete a list of lists from the database
			Route::delete('/', 'Admin\ListsController@delete');

			// GET /
			// return a list with all user's lists
			Route::get('/', 'Admin\ListsController@fetch');
		});

		// Users
		Route::prefix('/users')->group( function () {

			// POST /loginas/{id}
			Route::post('/loginas/{id}', 'Admin\UsersController@loginAsThisUser');

			// PUT /unsuspend
			// unsuspend user accounts; set "active" user flag to "1"
			Route::put('/unsuspend', ['uses' => 'Admin\UsersController@toggleAccounts', 'flagValue' => 1]);

			// PUT /suspend
			// suspend user accounts; set "active" user flag to "0"
			Route::put('/suspend', ['uses' => 'Admin\UsersController@toggleAccounts', 'flagValue' => 0]);

			// DELETE /
			// delete a list of users from the database
			Route::delete('/', 'Admin\UsersController@delete');

			// GET /
			// return a list with all users
			Route::get('/', 'Admin\UsersController@fetch');
		});

		// Dashboard
		Route::prefix('/dashboard')->group( function () {

			// POST /massemailing
			Route::post('/massemailing', 'Admin\DashboardController@massEmailing');

			// GET /emailshistory
			// return mass e-mails history statistics
			Route::get('/emailshistory', 'Admin\DashboardController@fetchEmailsHistory');

			// GET /statistics
			// return basic usage statistics
			Route::get('/statistics', 'Admin\DashboardController@fetchStatistics');
		});
	});
});


// ------------------------------------------------------------------------
// User
Route::prefix('/user')->group( function () {

	// POST /logout
	// no real need to send a request to the server for this
	// nothing happens on server side
	// Route::post('/logout', 'User\LogoutController@index');

	// POST /login
	Route::post('/login', 'User\LoginController@index');

	// PUT /resetpassword
	// change the user password
	Route::put('/resetpassword', 'User\ResetPasswordController@index');

	// POST /forgottenpassword
	// request a password reset
	Route::post('/forgottenpassword', 'User\ForgottenPasswordController@index');

	// POST /register
	// register a new user
	Route::post('/register', 'User\RegisterController@index');

	// POST /register
	// verify a new user registration
	Route::post('/register/verify', 'User\RegisterController@verifyAccount');

	// Authentication required
	Route::group(['middleware' => ['auth.check', 'auth.role:user,admin']], function () {

		// PUT /changepassword
		// change the user password
		Route::put('/changepassword', 'UserController@changePassword');

		// POST /profileimage
		// update the user's profile photo
		Route::post('/profileimage', 'UserController@updateProfilePhoto');

		// DELETE /profileimage
		// delete user profile photo
		Route::delete('/profileimage', 'UserController@deleteProfilePhoto');

		// POST /init
		// this is the login validation request, the very first request from the client
		// the response might include some additional data if the user is authenticated
		Route::post('/init', 'InitController@index');

		// GET /
		// User Profile Info
		Route::get('/', 'UserController@getProfileData');

		// Tasks
		Route::prefix('/tasks')->group( function () {

			// PUT /order
			Route::put('/order', 'TasksController@updateOrder');

			// POST /
			// adds a new task to the database
			Route::post('/', 'TasksController@add');

			// DELETE /{id}
			// removes a task from the database
			Route::delete('/{id}', 'TasksController@delete');

			// PUT /rename
			Route::put('/rename', 'TasksController@rename');

			// PUT /complete
			Route::put('/complete', 'TasksController@complete');
		});

		// Lists
		Route::prefix('/lists')->group( function () {

			// PUT /order
			Route::put('/order', 'ListsController@updateOrder');

			// GET /
			// returns the user's lists, including their tasks
			Route::get('/', 'ListsController@getUserListsWithTasks');

			// GET /templates
			Route::get('/templates', 'ListsController@getTemplates');

			// POST /
			// adds a new list to the database
			Route::post('/', 'ListsController@add');

			// PUT /
			// partialy updates an existing list
			Route::put('/', 'ListsController@partialUpdate');

			// PUT /hidecompleteditems/{id}
			// toggles the specified list's "hide_completed_items" flag
			Route::put('/hidecompleteditems/{id}', 'ListsController@toggleCompletedTasksVisibility');

			// DELETE /{id}
			// removes a list from the database
			Route::delete('/{id}', 'ListsController@deleteList');
		});
	});
});

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});
*/
